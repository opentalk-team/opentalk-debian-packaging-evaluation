# Dependency tree of `opentalk-controller`

The dependency tree of the OpenTalk controller has some circular dev
dependencies that make it a little hard to analyze. So we have to cut some edges
in the analysis to get a rough idea of how this tree could be packaged.

The tool of choice for analyzing the dependency tree would be `cargo debstatus`,
but even though the documentation suggests through the `--all` parameter that
duplicate dependencies are only listed once by default that appears to not be
the case.

In order to avoid the dependency cycles that cause the command to run forever
in a recursion and produce inifinite output, we use the `--no-dev-dependencies`
parameter. This might cause some inaccurate information about what needs to
be packaged, because depending on the use case, most dev dependencies usually
should go into the Debian repository as well unless patched out for good reason.

Please beware that the dependency tree changes over time. This analysis was
performed on the `v0.10.0` tag of the source tree.

## Analysis with `cargo debstatus`

We needed to use some trickery to make the list unique due to `cargo debstatus` not
removing duplicates.

`cargo debstatus` prints a `🔴` for each crate that has not been uploaded to Debian yet,
and a `⌛` for every crate that is available, but outdated for the current dependency tree.

Example output looks like this (shortened):

```
 🔴 opentalk-controller v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/main)
 🔴 ├── actix-web v4.5.1
 🔴 │   ├── actix-codec v0.5.2
    │   │   ├── bitflags v2.4.2 (in debian)
    .   .   .
    .   .   .
    │   │   └── tracing v0.1.40 (in debian)
 🔴 │   ├── actix-http v3.6.0
    .   .   .
    .   .   .
    .   .   .
    │   │   ├── tracing v0.1.40 (in debian)
 ⌛ │   │   └── zstd v0.13.0 (outdated, 0.12.1 in debian)
 ⌛ │   │       └── zstd-safe v7.0.0 (outdated, 6.0.2 in debian)
    │   │           └── zstd-sys v2.0.9+zstd.1.5.5 (in debian)
```

The command used is `cargo debstatus --no-dev-dependencies --no-indent | grep "🔴" | sort | uniq`,
of course we only see crates that haven't been uploaded at all. There might be some outdated
packages as well which we will ignore for now, but they could of course easily be identified by
grepping for `⌛` instead.

Output:

```
 🔴 actix-codec v0.5.2
 🔴 actix-cors v0.7.0
 🔴 actix-http v3.6.0
 🔴 actix-router v0.5.2
 🔴 actix-server v2.3.0
 🔴 actix-service v2.0.2
 🔴 actix-tls v3.3.0
 🔴 actix-utils v3.0.1
 🔴 actix v0.13.3
 🔴 actix-web-actors v4.3.0
 🔴 actix-web-codegen v4.2.2
 🔴 actix-web-httpauth v0.8.1
 🔴 actix-web v4.5.1
 🔴 amq-protocol-tcp v7.1.2
 🔴 amq-protocol-types v7.1.2
 🔴 amq-protocol-uri v7.1.2
 🔴 amq-protocol v7.1.2
 🔴 async-global-executor-trait v2.1.0
 🔴 async-reactor-trait v1.1.0
 🔴 aws-credential-types v1.1.5
 🔴 aws-runtime v1.1.5
 🔴 aws-sdk-s3 v1.15.0
 🔴 aws-sigv4 v1.1.5
 🔴 aws-smithy-async v1.1.6
 🔴 aws-smithy-checksums v0.60.5
 🔴 aws-smithy-eventstream v0.60.4
 🔴 aws-smithy-http v0.60.5
 🔴 aws-smithy-json v0.60.5
 🔴 aws-smithy-runtime-api v1.1.6
 🔴 aws-smithy-runtime v1.1.6
 🔴 aws-smithy-types v1.1.6
 🔴 aws-smithy-xml v0.60.5
 🔴 aws-types v1.1.5
 🔴 base64-simd v0.8.0
 🔴 bytestring v1.3.1
 🔴 bytes-utils v0.1.4
 🔴 casbin v2.0.9 (git+https://github.com/kbalt/casbin-rs.git?rev=a5cd79704726b423030942769779ea7d4e8129ee#a5cd79704726b423030942769779ea7d4e8129ee)
 🔴 cidr v0.2.2
 🔴 crc32c v0.6.5
 🔴 deadpool-runtime v0.1.3
 🔴 deadpool v0.9.5
 🔴 diesel-async v0.3.2
 🔴 digest_auth v0.3.1
 🔴 displaydoc v0.2.4
 🔴 ed25519-dalek v2.1.1
 🔴 email_address v0.2.4
 🔴 executor-trait v2.1.0
 🔴 http-request-derive-macros v0.1.0
 🔴 http-request-derive v0.1.1
 🔴 impl-more v0.1.6
 🔴 kustos-shared v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos-shared)
 🔴 kustos v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos)
 🔴 lapin-pool v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/lapin-pool)
 🔴 lapin v2.3.1
 🔴 local-channel v0.1.5
 🔴 local-waker v0.1.4
 🔴 moka v0.12.5
 🔴 mutually_exclusive_features v0.0.3
 🔴 oncemutex v0.1.1
 🔴 openidconnect v3.4.0
 🔴 opentalk-cache v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/cache)
 🔴 opentalk-chat v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/chat)
 🔴 opentalk-client-shared v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/client-shared)
 🔴 opentalk-community-modules v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/community-modules)
 🔴 opentalk-controller-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-core)
 🔴 opentalk-controller-settings v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-settings)
 🔴 opentalk-controller-utils v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-utils)
 🔴 opentalk-controller v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/main)
 🔴 opentalk-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/core)
 🔴 opentalk-database v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/database)
 🔴 opentalk-db-storage v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/db-storage)
 🔴 opentalk-diesel-newtype-impl v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/diesel-newtype-impl)
 🔴 opentalk-diesel-newtype v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/diesel-newtype)
 🔴 opentalk-etherpad-client v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/etherpad-client)
 🔴 opentalk-integration v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/integration)
 🔴 opentalk-janus-client v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/janus-client)
 🔴 opentalk-janus-media v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/janus-media)
 🔴 opentalk-jobs v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/jobs)
 🔴 opentalk-keycloak-admin v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/keycloak-admin)
 🔴 opentalk-kustos-prefix-impl v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos-prefix-impl)
 🔴 opentalk-kustos-prefix v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos-prefix)
 🔴 opentalk-log v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/log)
 🔴 opentalk-mail-worker-protocol v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/mail-worker-protocol)
 🔴 opentalk-nextcloud-client v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/nextcloud-client)
 🔴 opentalk-polls v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/polls)
 🔴 opentalk-proc-macro-fields-helper v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/proc-macro-fields-helper)
 🔴 opentalk-protocol v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/protocol)
 🔴 opentalk-r3dlock v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/r3dlock)
 🔴 opentalk-shared-folder v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/shared-folder)
 🔴 opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core)
 🔴 opentalk-timer v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/timer)
 🔴 opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types)
 🔴 opentalk-whiteboard v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/whiteboard)
 🔴 opentelemetry-otlp v0.14.0
 🔴 opentelemetry-prometheus v0.14.1
 🔴 opentelemetry-proto v0.4.0
 🔴 opentelemetry_sdk v0.21.2
 🔴 opentelemetry-semantic-conventions v0.13.0
 🔴 opentelemetry v0.21.0
 🔴 outref v0.5.1
 🔴 phonenumber v0.3.3+8.13.9
 🔴 pinky-swear v6.2.0
 🔴 quanta v0.12.2
 🔴 raw-cpuid v11.0.1
 🔴 reactor-trait v1.1.0
 🔴 redis-args-impl v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args-impl)
 🔴 redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args)
 🔴 redis v0.24.0
 🔴 refinery-core v0.8.12
 🔴 refinery-macros v0.8.12
 🔴 refinery v0.8.12
 🔴 regex-cache v0.2.1
 🔴 regex-lite v0.1.5
 🔴 reqwest_dav v0.1.9
 🔴 ritelinked v0.3.2
 🔴 rrule v0.11.0
 🔴 scoped-futures v0.1.3
 🔴 serde_plain v1.0.2
 🔴 serde_url_params v0.2.1
 🔴 tagptr v0.2.0
 🔴 tcp-stream v0.26.1
 🔴 tokio-executor-trait v2.1.1
 🔴 tokio-reactor-trait v1.1.0
 🔴 tokio-retry v0.3.0
 🔴 tracing-actix-web v0.7.9
 🔴 tracing-opentelemetry v0.22.0
 🔴 validator_derive v0.16.0
 🔴 validator_types v0.16.0
 🔴 validator v0.16.1
 🔴 vsimd v0.8.0
 🔴 webpki-roots v0.25.4
```

## Output of `cargo tree`

The `cargo tree` command gives us a rough overview of the dependency tree, but
does not show any information about what has been uploaded to Debian already.

```
opentalk-controller v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/main)
├── actix-web v4.5.1
│   ├── actix-codec v0.5.2
│   │   ├── bitflags v2.4.2
│   │   ├── bytes v1.5.0
│   │   ├── futures-core v0.3.30
│   │   ├── futures-sink v0.3.30
│   │   ├── memchr v2.7.1
│   │   ├── pin-project-lite v0.2.13
│   │   ├── tokio v1.36.0
│   │   │   ├── bytes v1.5.0
│   │   │   ├── libc v0.2.153
│   │   │   ├── mio v0.8.10
│   │   │   │   ├── libc v0.2.153
│   │   │   │   └── log v0.4.20
│   │   │   │       └── serde v1.0.196
│   │   │   │           └── serde_derive v1.0.196 (proc-macro)
│   │   │   │               ├── proc-macro2 v1.0.78
│   │   │   │               │   └── unicode-ident v1.0.12
│   │   │   │               ├── quote v1.0.35
│   │   │   │               │   └── proc-macro2 v1.0.78 (*)
│   │   │   │               └── syn v2.0.49
│   │   │   │                   ├── proc-macro2 v1.0.78 (*)
│   │   │   │                   ├── quote v1.0.35 (*)
│   │   │   │                   └── unicode-ident v1.0.12
│   │   │   ├── num_cpus v1.16.0
│   │   │   │   └── libc v0.2.153
│   │   │   ├── parking_lot v0.12.1
│   │   │   │   ├── lock_api v0.4.11
│   │   │   │   │   └── scopeguard v1.2.0
│   │   │   │   │   [build-dependencies]
│   │   │   │   │   └── autocfg v1.1.0
│   │   │   │   └── parking_lot_core v0.9.9
│   │   │   │       ├── cfg-if v1.0.0
│   │   │   │       ├── libc v0.2.153
│   │   │   │       └── smallvec v1.13.1
│   │   │   │           └── serde v1.0.196 (*)
│   │   │   ├── pin-project-lite v0.2.13
│   │   │   ├── signal-hook-registry v1.4.1
│   │   │   │   └── libc v0.2.153
│   │   │   ├── socket2 v0.5.5
│   │   │   │   └── libc v0.2.153
│   │   │   └── tokio-macros v2.2.0 (proc-macro)
│   │   │       ├── proc-macro2 v1.0.78 (*)
│   │   │       ├── quote v1.0.35 (*)
│   │   │       └── syn v2.0.49 (*)
│   │   ├── tokio-util v0.7.10
│   │   │   ├── bytes v1.5.0
│   │   │   ├── futures-core v0.3.30
│   │   │   ├── futures-sink v0.3.30
│   │   │   ├── pin-project-lite v0.2.13
│   │   │   ├── tokio v1.36.0 (*)
│   │   │   └── tracing v0.1.40
│   │   │       ├── log v0.4.20 (*)
│   │   │       ├── pin-project-lite v0.2.13
│   │   │       ├── tracing-attributes v0.1.27 (proc-macro)
│   │   │       │   ├── proc-macro2 v1.0.78 (*)
│   │   │       │   ├── quote v1.0.35 (*)
│   │   │       │   └── syn v2.0.49 (*)
│   │   │       └── tracing-core v0.1.32
│   │   │           └── once_cell v1.19.0
│   │   └── tracing v0.1.40 (*)
│   ├── actix-http v3.6.0
│   │   ├── actix-codec v0.5.2 (*)
│   │   ├── actix-rt v2.9.0
│   │   │   ├── actix-macros v0.2.4 (proc-macro)
│   │   │   │   ├── quote v1.0.35 (*)
│   │   │   │   └── syn v2.0.49 (*)
│   │   │   ├── futures-core v0.3.30
│   │   │   └── tokio v1.36.0 (*)
│   │   ├── actix-service v2.0.2
│   │   │   ├── futures-core v0.3.30
│   │   │   ├── paste v1.0.14 (proc-macro)
│   │   │   └── pin-project-lite v0.2.13
│   │   ├── actix-tls v3.3.0
│   │   │   ├── actix-rt v2.9.0 (*)
│   │   │   ├── actix-service v2.0.2 (*)
│   │   │   ├── actix-utils v3.0.1
│   │   │   │   ├── local-waker v0.1.4
│   │   │   │   └── pin-project-lite v0.2.13
│   │   │   ├── futures-core v0.3.30
│   │   │   ├── impl-more v0.1.6
│   │   │   ├── pin-project-lite v0.2.13
│   │   │   ├── tokio v1.36.0 (*)
│   │   │   ├── tokio-rustls v0.24.1
│   │   │   │   ├── rustls v0.21.10
│   │   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   │   ├── ring v0.17.7
│   │   │   │   │   │   ├── getrandom v0.2.12
│   │   │   │   │   │   │   ├── cfg-if v1.0.0
│   │   │   │   │   │   │   └── libc v0.2.153
│   │   │   │   │   │   ├── spin v0.9.8
│   │   │   │   │   │   │   └── lock_api v0.4.11 (*)
│   │   │   │   │   │   └── untrusted v0.9.0
│   │   │   │   │   │   [build-dependencies]
│   │   │   │   │   │   └── cc v1.0.83
│   │   │   │   │   │       ├── jobserver v0.1.28
│   │   │   │   │   │       │   └── libc v0.2.153
│   │   │   │   │   │       └── libc v0.2.153
│   │   │   │   │   ├── rustls-webpki v0.101.7
│   │   │   │   │   │   ├── ring v0.17.7 (*)
│   │   │   │   │   │   └── untrusted v0.9.0
│   │   │   │   │   └── sct v0.7.1
│   │   │   │   │       ├── ring v0.17.7 (*)
│   │   │   │   │       └── untrusted v0.9.0
│   │   │   │   └── tokio v1.36.0 (*)
│   │   │   ├── tokio-util v0.7.10 (*)
│   │   │   ├── tracing v0.1.40 (*)
│   │   │   └── webpki-roots v0.25.4
│   │   ├── actix-utils v3.0.1 (*)
│   │   ├── ahash v0.8.8
│   │   │   ├── cfg-if v1.0.0
│   │   │   ├── const-random v0.1.17
│   │   │   │   └── const-random-macro v0.1.16 (proc-macro)
│   │   │   │       ├── getrandom v0.2.12
│   │   │   │       │   ├── cfg-if v1.0.0
│   │   │   │       │   └── libc v0.2.153
│   │   │   │       ├── once_cell v1.19.0
│   │   │   │       └── tiny-keccak v2.0.2
│   │   │   │           └── crunchy v0.2.2
│   │   │   ├── getrandom v0.2.12 (*)
│   │   │   ├── once_cell v1.19.0
│   │   │   └── zerocopy v0.7.32
│   │   │   [build-dependencies]
│   │   │   └── version_check v0.9.4
│   │   ├── base64 v0.21.7
│   │   ├── bitflags v2.4.2
│   │   ├── brotli v3.4.0
│   │   │   ├── alloc-no-stdlib v2.0.4
│   │   │   ├── alloc-stdlib v0.2.2
│   │   │   │   └── alloc-no-stdlib v2.0.4
│   │   │   └── brotli-decompressor v2.5.1
│   │   │       ├── alloc-no-stdlib v2.0.4
│   │   │       └── alloc-stdlib v0.2.2 (*)
│   │   ├── bytes v1.5.0
│   │   ├── bytestring v1.3.1
│   │   │   ├── bytes v1.5.0
│   │   │   └── serde v1.0.196 (*)
│   │   ├── derive_more v0.99.17 (proc-macro)
│   │   │   ├── convert_case v0.4.0
│   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   ├── quote v1.0.35 (*)
│   │   │   └── syn v1.0.109
│   │   │       ├── proc-macro2 v1.0.78 (*)
│   │   │       ├── quote v1.0.35 (*)
│   │   │       └── unicode-ident v1.0.12
│   │   │   [build-dependencies]
│   │   │   └── rustc_version v0.4.0
│   │   │       └── semver v1.0.21
│   │   │           └── serde v1.0.196
│   │   │               └── serde_derive v1.0.196 (proc-macro) (*)
│   │   ├── encoding_rs v0.8.33
│   │   │   └── cfg-if v1.0.0
│   │   ├── flate2 v1.0.28
│   │   │   ├── crc32fast v1.4.0
│   │   │   │   └── cfg-if v1.0.0
│   │   │   └── miniz_oxide v0.7.2
│   │   │       └── adler v1.0.2
│   │   ├── futures-core v0.3.30
│   │   ├── h2 v0.3.24
│   │   │   ├── bytes v1.5.0
│   │   │   ├── fnv v1.0.7
│   │   │   ├── futures-core v0.3.30
│   │   │   ├── futures-sink v0.3.30
│   │   │   ├── futures-util v0.3.30
│   │   │   │   ├── futures-channel v0.3.30
│   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   └── futures-sink v0.3.30
│   │   │   │   ├── futures-core v0.3.30
│   │   │   │   ├── futures-io v0.3.30
│   │   │   │   ├── futures-macro v0.3.30 (proc-macro)
│   │   │   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   ├── quote v1.0.35 (*)
│   │   │   │   │   └── syn v2.0.49 (*)
│   │   │   │   ├── futures-sink v0.3.30
│   │   │   │   ├── futures-task v0.3.30
│   │   │   │   ├── memchr v2.7.1
│   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   ├── pin-utils v0.1.0
│   │   │   │   └── slab v0.4.9
│   │   │   │       [build-dependencies]
│   │   │   │       └── autocfg v1.1.0
│   │   │   ├── http v0.2.11
│   │   │   │   ├── bytes v1.5.0
│   │   │   │   ├── fnv v1.0.7
│   │   │   │   └── itoa v1.0.10
│   │   │   ├── indexmap v2.2.3
│   │   │   │   ├── equivalent v1.0.1
│   │   │   │   └── hashbrown v0.14.3
│   │   │   ├── slab v0.4.9 (*)
│   │   │   ├── tokio v1.36.0 (*)
│   │   │   ├── tokio-util v0.7.10 (*)
│   │   │   └── tracing v0.1.40 (*)
│   │   ├── http v0.2.11 (*)
│   │   ├── httparse v1.8.0
│   │   ├── httpdate v1.0.3
│   │   ├── itoa v1.0.10
│   │   ├── language-tags v0.3.2
│   │   ├── local-channel v0.1.5
│   │   │   ├── futures-core v0.3.30
│   │   │   ├── futures-sink v0.3.30
│   │   │   └── local-waker v0.1.4
│   │   ├── mime v0.3.17
│   │   ├── percent-encoding v2.3.1
│   │   ├── pin-project-lite v0.2.13
│   │   ├── rand v0.8.5
│   │   │   ├── libc v0.2.153
│   │   │   ├── rand_chacha v0.3.1
│   │   │   │   ├── ppv-lite86 v0.2.17
│   │   │   │   └── rand_core v0.6.4
│   │   │   │       └── getrandom v0.2.12 (*)
│   │   │   └── rand_core v0.6.4 (*)
│   │   ├── sha1 v0.10.6
│   │   │   ├── cfg-if v1.0.0
│   │   │   ├── cpufeatures v0.2.12
│   │   │   └── digest v0.10.7
│   │   │       ├── block-buffer v0.10.4
│   │   │       │   └── generic-array v0.14.7
│   │   │       │       ├── typenum v1.17.0
│   │   │       │       └── zeroize v1.7.0
│   │   │       │       [build-dependencies]
│   │   │       │       └── version_check v0.9.4
│   │   │       ├── const-oid v0.9.6
│   │   │       ├── crypto-common v0.1.6
│   │   │       │   ├── generic-array v0.14.7 (*)
│   │   │       │   └── typenum v1.17.0
│   │   │       └── subtle v2.5.0
│   │   ├── smallvec v1.13.1 (*)
│   │   ├── tokio v1.36.0 (*)
│   │   ├── tokio-util v0.7.10 (*)
│   │   ├── tracing v0.1.40 (*)
│   │   └── zstd v0.13.0
│   │       └── zstd-safe v7.0.0
│   │           └── zstd-sys v2.0.9+zstd.1.5.5
│   │               [build-dependencies]
│   │               ├── cc v1.0.83 (*)
│   │               └── pkg-config v0.3.30
│   ├── actix-macros v0.2.4 (proc-macro) (*)
│   ├── actix-router v0.5.2
│   │   ├── bytestring v1.3.1 (*)
│   │   ├── http v0.2.11 (*)
│   │   ├── regex v1.10.3
│   │   │   ├── aho-corasick v1.1.2
│   │   │   │   └── memchr v2.7.1
│   │   │   ├── memchr v2.7.1
│   │   │   ├── regex-automata v0.4.5
│   │   │   │   ├── aho-corasick v1.1.2 (*)
│   │   │   │   ├── memchr v2.7.1
│   │   │   │   └── regex-syntax v0.8.2
│   │   │   └── regex-syntax v0.8.2
│   │   ├── serde v1.0.196 (*)
│   │   └── tracing v0.1.40 (*)
│   ├── actix-rt v2.9.0 (*)
│   ├── actix-server v2.3.0
│   │   ├── actix-rt v2.9.0 (*)
│   │   ├── actix-service v2.0.2 (*)
│   │   ├── actix-utils v3.0.1 (*)
│   │   ├── futures-core v0.3.30
│   │   ├── futures-util v0.3.30 (*)
│   │   ├── mio v0.8.10 (*)
│   │   ├── socket2 v0.5.5 (*)
│   │   ├── tokio v1.36.0 (*)
│   │   └── tracing v0.1.40 (*)
│   ├── actix-service v2.0.2 (*)
│   ├── actix-tls v3.3.0 (*)
│   ├── actix-utils v3.0.1 (*)
│   ├── actix-web-codegen v4.2.2 (proc-macro)
│   │   ├── actix-router v0.5.2 (*)
│   │   ├── proc-macro2 v1.0.78 (*)
│   │   ├── quote v1.0.35 (*)
│   │   └── syn v2.0.49 (*)
│   ├── ahash v0.8.8 (*)
│   ├── bytes v1.5.0
│   ├── bytestring v1.3.1 (*)
│   ├── cfg-if v1.0.0
│   ├── cookie v0.16.2
│   │   ├── percent-encoding v2.3.1
│   │   └── time v0.3.34
│   │       ├── deranged v0.3.11
│   │       │   └── powerfmt v0.2.0
│   │       ├── itoa v1.0.10
│   │       ├── num-conv v0.1.0
│   │       ├── powerfmt v0.2.0
│   │       ├── time-core v0.1.2
│   │       └── time-macros v0.2.17 (proc-macro)
│   │           ├── num-conv v0.1.0
│   │           └── time-core v0.1.2
│   │   [build-dependencies]
│   │   └── version_check v0.9.4
│   ├── derive_more v0.99.17 (proc-macro) (*)
│   ├── encoding_rs v0.8.33 (*)
│   ├── futures-core v0.3.30
│   ├── futures-util v0.3.30 (*)
│   ├── itoa v1.0.10
│   ├── language-tags v0.3.2
│   ├── log v0.4.20 (*)
│   ├── mime v0.3.17
│   ├── once_cell v1.19.0
│   ├── pin-project-lite v0.2.13
│   ├── regex v1.10.3 (*)
│   ├── serde v1.0.196 (*)
│   ├── serde_json v1.0.113
│   │   ├── itoa v1.0.10
│   │   ├── ryu v1.0.16
│   │   └── serde v1.0.196 (*)
│   ├── serde_urlencoded v0.7.1
│   │   ├── form_urlencoded v1.2.1
│   │   │   └── percent-encoding v2.3.1
│   │   ├── itoa v1.0.10
│   │   ├── ryu v1.0.16
│   │   └── serde v1.0.196 (*)
│   ├── smallvec v1.13.1 (*)
│   ├── socket2 v0.5.5 (*)
│   ├── time v0.3.34 (*)
│   └── url v2.5.0
│       ├── form_urlencoded v1.2.1 (*)
│       ├── idna v0.5.0
│       │   ├── unicode-bidi v0.3.15
│       │   └── unicode-normalization v0.1.22
│       │       └── tinyvec v1.6.0
│       │           └── tinyvec_macros v0.1.1
│       ├── percent-encoding v2.3.1
│       └── serde v1.0.196 (*)
├── anstream v0.6.11
│   ├── anstyle v1.0.6
│   ├── anstyle-parse v0.2.3
│   │   └── utf8parse v0.2.1
│   ├── anstyle-query v1.0.2
│   ├── colorchoice v1.0.0
│   └── utf8parse v0.2.1
├── anyhow v1.0.79
├── opentalk-community-modules v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/community-modules)
│   ├── anyhow v1.0.79
│   ├── async-trait v0.1.77 (proc-macro)
│   │   ├── proc-macro2 v1.0.78 (*)
│   │   ├── quote v1.0.35 (*)
│   │   └── syn v2.0.49 (*)
│   ├── opentalk-chat v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/chat)
│   │   ├── anyhow v1.0.79
│   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   ├── chrono v0.4.34
│   │   │   ├── iana-time-zone v0.1.60
│   │   │   ├── num-traits v0.2.18
│   │   │   │   └── libm v0.2.8
│   │   │   │   [build-dependencies]
│   │   │   │   └── autocfg v1.1.0
│   │   │   └── serde v1.0.196 (*)
│   │   ├── log v0.4.20 (*)
│   │   ├── opentalk-database v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/database)
│   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   ├── deadpool-runtime v0.1.3
│   │   │   │   └── tokio v1.36.0 (*)
│   │   │   ├── diesel v2.1.4
│   │   │   │   ├── bitflags v2.4.2
│   │   │   │   ├── byteorder v1.5.0
│   │   │   │   ├── chrono v0.4.34 (*)
│   │   │   │   ├── diesel_derives v2.1.2 (proc-macro)
│   │   │   │   │   ├── diesel_table_macro_syntax v0.1.0
│   │   │   │   │   │   └── syn v2.0.49 (*)
│   │   │   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   ├── quote v1.0.35 (*)
│   │   │   │   │   └── syn v2.0.49 (*)
│   │   │   │   ├── itoa v1.0.10
│   │   │   │   ├── pq-sys v0.4.8
│   │   │   │   ├── serde_json v1.0.113 (*)
│   │   │   │   └── uuid v1.7.0
│   │   │   │       ├── getrandom v0.2.12 (*)
│   │   │   │       └── serde v1.0.196 (*)
│   │   │   ├── diesel-async v0.3.2
│   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   ├── deadpool v0.9.5
│   │   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   │   ├── deadpool-runtime v0.1.3 (*)
│   │   │   │   │   ├── num_cpus v1.16.0 (*)
│   │   │   │   │   ├── retain_mut v0.1.9
│   │   │   │   │   └── tokio v1.36.0 (*)
│   │   │   │   ├── diesel v2.1.4 (*)
│   │   │   │   ├── futures-util v0.3.30 (*)
│   │   │   │   ├── scoped-futures v0.1.3
│   │   │   │   │   ├── cfg-if v1.0.0
│   │   │   │   │   └── pin-utils v0.1.0
│   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   └── tokio-postgres v0.7.10
│   │   │   │       ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │       ├── byteorder v1.5.0
│   │   │   │       ├── bytes v1.5.0
│   │   │   │       ├── fallible-iterator v0.2.0
│   │   │   │       ├── futures-channel v0.3.30 (*)
│   │   │   │       ├── futures-util v0.3.30 (*)
│   │   │   │       ├── log v0.4.20 (*)
│   │   │   │       ├── parking_lot v0.12.1 (*)
│   │   │   │       ├── percent-encoding v2.3.1
│   │   │   │       ├── phf v0.11.2
│   │   │   │       │   └── phf_shared v0.11.2
│   │   │   │       │       └── siphasher v0.3.11
│   │   │   │       ├── pin-project-lite v0.2.13
│   │   │   │       ├── postgres-protocol v0.6.6
│   │   │   │       │   ├── base64 v0.21.7
│   │   │   │       │   ├── byteorder v1.5.0
│   │   │   │       │   ├── bytes v1.5.0
│   │   │   │       │   ├── fallible-iterator v0.2.0
│   │   │   │       │   ├── hmac v0.12.1
│   │   │   │       │   │   └── digest v0.10.7 (*)
│   │   │   │       │   ├── md-5 v0.10.6
│   │   │   │       │   │   ├── cfg-if v1.0.0
│   │   │   │       │   │   └── digest v0.10.7 (*)
│   │   │   │       │   ├── memchr v2.7.1
│   │   │   │       │   ├── rand v0.8.5 (*)
│   │   │   │       │   ├── sha2 v0.10.8
│   │   │   │       │   │   ├── cfg-if v1.0.0
│   │   │   │       │   │   ├── cpufeatures v0.2.12
│   │   │   │       │   │   └── digest v0.10.7 (*)
│   │   │   │       │   └── stringprep v0.1.4
│   │   │   │       │       ├── finl_unicode v1.2.0
│   │   │   │       │       ├── unicode-bidi v0.3.15
│   │   │   │       │       └── unicode-normalization v0.1.22 (*)
│   │   │   │       ├── postgres-types v0.2.6
│   │   │   │       │   ├── bytes v1.5.0
│   │   │   │       │   ├── fallible-iterator v0.2.0
│   │   │   │       │   └── postgres-protocol v0.6.6 (*)
│   │   │   │       ├── rand v0.8.5 (*)
│   │   │   │       ├── socket2 v0.5.5 (*)
│   │   │   │       ├── tokio v1.36.0 (*)
│   │   │   │       ├── tokio-util v0.7.10 (*)
│   │   │   │       └── whoami v1.4.1
│   │   │   ├── futures-core v0.3.30
│   │   │   ├── log v0.4.20 (*)
│   │   │   ├── opentalk-controller-settings v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-settings)
│   │   │   │   ├── anstream v0.6.11 (*)
│   │   │   │   ├── anyhow v1.0.79
│   │   │   │   ├── arc-swap v1.6.0
│   │   │   │   ├── cidr v0.2.2
│   │   │   │   │   └── serde v1.0.196 (*)
│   │   │   │   ├── config v0.14.0
│   │   │   │   │   ├── lazy_static v1.4.0
│   │   │   │   │   │   └── spin v0.5.2
│   │   │   │   │   ├── nom v7.1.3
│   │   │   │   │   │   ├── memchr v2.7.1
│   │   │   │   │   │   └── minimal-lexical v0.2.1
│   │   │   │   │   ├── pathdiff v0.2.1
│   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   └── toml v0.8.10
│   │   │   │   │       ├── serde v1.0.196 (*)
│   │   │   │   │       ├── serde_spanned v0.6.5
│   │   │   │   │       │   └── serde v1.0.196 (*)
│   │   │   │   │       ├── toml_datetime v0.6.5
│   │   │   │   │       │   └── serde v1.0.196 (*)
│   │   │   │   │       └── toml_edit v0.22.6
│   │   │   │   │           ├── indexmap v2.2.3 (*)
│   │   │   │   │           ├── serde v1.0.196 (*)
│   │   │   │   │           ├── serde_spanned v0.6.5 (*)
│   │   │   │   │           ├── toml_datetime v0.6.5 (*)
│   │   │   │   │           └── winnow v0.6.1
│   │   │   │   ├── openidconnect v3.4.0
│   │   │   │   │   ├── base64 v0.13.1
│   │   │   │   │   ├── chrono v0.4.34 (*)
│   │   │   │   │   ├── dyn-clone v1.0.16
│   │   │   │   │   ├── ed25519-dalek v2.1.1
│   │   │   │   │   │   ├── curve25519-dalek v4.1.2
│   │   │   │   │   │   │   ├── cfg-if v1.0.0
│   │   │   │   │   │   │   ├── cpufeatures v0.2.12
│   │   │   │   │   │   │   ├── curve25519-dalek-derive v0.1.1 (proc-macro)
│   │   │   │   │   │   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   │   │   │   ├── quote v1.0.35 (*)
│   │   │   │   │   │   │   │   └── syn v2.0.49 (*)
│   │   │   │   │   │   │   ├── digest v0.10.7 (*)
│   │   │   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   │   │   [build-dependencies]
│   │   │   │   │   │   │   ├── platforms v3.3.0
│   │   │   │   │   │   │   └── rustc_version v0.4.0 (*)
│   │   │   │   │   │   ├── ed25519 v2.2.3
│   │   │   │   │   │   │   ├── pkcs8 v0.10.2
│   │   │   │   │   │   │   │   ├── der v0.7.8
│   │   │   │   │   │   │   │   │   ├── const-oid v0.9.6
│   │   │   │   │   │   │   │   │   ├── pem-rfc7468 v0.7.0
│   │   │   │   │   │   │   │   │   │   └── base64ct v1.6.0
│   │   │   │   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   │   │   │   └── spki v0.7.3
│   │   │   │   │   │   │   │       └── der v0.7.8 (*)
│   │   │   │   │   │   │   └── signature v2.2.0
│   │   │   │   │   │   │       ├── digest v0.10.7 (*)
│   │   │   │   │   │   │       └── rand_core v0.6.4 (*)
│   │   │   │   │   │   ├── sha2 v0.10.8 (*)
│   │   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   ├── hmac v0.12.1 (*)
│   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   ├── itertools v0.10.5
│   │   │   │   │   │   └── either v1.10.0
│   │   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   │   ├── oauth2 v4.4.2
│   │   │   │   │   │   ├── base64 v0.13.1
│   │   │   │   │   │   ├── chrono v0.4.34 (*)
│   │   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   │   ├── rand v0.8.5 (*)
│   │   │   │   │   │   ├── reqwest v0.11.24
│   │   │   │   │   │   │   ├── base64 v0.21.7
│   │   │   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   │   │   ├── cookie v0.17.0
│   │   │   │   │   │   │   │   ├── percent-encoding v2.3.1
│   │   │   │   │   │   │   │   └── time v0.3.34 (*)
│   │   │   │   │   │   │   │   [build-dependencies]
│   │   │   │   │   │   │   │   └── version_check v0.9.4
│   │   │   │   │   │   │   ├── cookie_store v0.20.0
│   │   │   │   │   │   │   │   ├── cookie v0.17.0 (*)
│   │   │   │   │   │   │   │   ├── idna v0.3.0
│   │   │   │   │   │   │   │   │   ├── unicode-bidi v0.3.15
│   │   │   │   │   │   │   │   │   └── unicode-normalization v0.1.22 (*)
│   │   │   │   │   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   │   │   │   │   ├── publicsuffix v2.2.3
│   │   │   │   │   │   │   │   │   ├── idna v0.3.0 (*)
│   │   │   │   │   │   │   │   │   └── psl-types v2.0.11
│   │   │   │   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   │   │   │   ├── serde_derive v1.0.196 (proc-macro) (*)
│   │   │   │   │   │   │   │   ├── serde_json v1.0.113 (*)
│   │   │   │   │   │   │   │   ├── time v0.3.34 (*)
│   │   │   │   │   │   │   │   └── url v2.5.0 (*)
│   │   │   │   │   │   │   ├── encoding_rs v0.8.33 (*)
│   │   │   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   │   │   ├── futures-util v0.3.30 (*)
│   │   │   │   │   │   │   ├── h2 v0.3.24 (*)
│   │   │   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   │   │   ├── http-body v0.4.6
│   │   │   │   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   │   │   │   └── pin-project-lite v0.2.13
│   │   │   │   │   │   │   ├── hyper v0.14.28
│   │   │   │   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   │   │   │   ├── futures-channel v0.3.30 (*)
│   │   │   │   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   │   │   │   ├── futures-util v0.3.30 (*)
│   │   │   │   │   │   │   │   ├── h2 v0.3.24 (*)
│   │   │   │   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   │   │   │   ├── http-body v0.4.6 (*)
│   │   │   │   │   │   │   │   ├── httparse v1.8.0
│   │   │   │   │   │   │   │   ├── httpdate v1.0.3
│   │   │   │   │   │   │   │   ├── itoa v1.0.10
│   │   │   │   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   │   │   │   ├── socket2 v0.5.5 (*)
│   │   │   │   │   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   │   │   │   │   ├── tower-service v0.3.2
│   │   │   │   │   │   │   │   ├── tracing v0.1.40 (*)
│   │   │   │   │   │   │   │   └── want v0.3.1
│   │   │   │   │   │   │   │       └── try-lock v0.2.5
│   │   │   │   │   │   │   ├── hyper-rustls v0.24.2
│   │   │   │   │   │   │   │   ├── futures-util v0.3.30 (*)
│   │   │   │   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   │   │   │   ├── hyper v0.14.28 (*)
│   │   │   │   │   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   │   │   │   │   ├── rustls v0.21.10 (*)
│   │   │   │   │   │   │   │   ├── rustls-native-certs v0.6.3
│   │   │   │   │   │   │   │   │   ├── openssl-probe v0.1.5
│   │   │   │   │   │   │   │   │   └── rustls-pemfile v1.0.4
│   │   │   │   │   │   │   │   │       └── base64 v0.21.7
│   │   │   │   │   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   │   │   │   │   └── tokio-rustls v0.24.1 (*)
│   │   │   │   │   │   │   ├── ipnet v2.9.0
│   │   │   │   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   │   │   │   ├── mime v0.3.17
│   │   │   │   │   │   │   ├── once_cell v1.19.0
│   │   │   │   │   │   │   ├── percent-encoding v2.3.1
│   │   │   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   │   │   ├── rustls v0.21.10 (*)
│   │   │   │   │   │   │   ├── rustls-native-certs v0.6.3 (*)
│   │   │   │   │   │   │   ├── rustls-pemfile v1.0.4 (*)
│   │   │   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   │   │   ├── serde_json v1.0.113 (*)
│   │   │   │   │   │   │   ├── serde_urlencoded v0.7.1 (*)
│   │   │   │   │   │   │   ├── sync_wrapper v0.1.2
│   │   │   │   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   │   │   │   ├── tokio-rustls v0.24.1 (*)
│   │   │   │   │   │   │   ├── tokio-util v0.7.10 (*)
│   │   │   │   │   │   │   ├── tower-service v0.3.2
│   │   │   │   │   │   │   ├── url v2.5.0 (*)
│   │   │   │   │   │   │   └── webpki-roots v0.25.4
│   │   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   │   ├── serde_json v1.0.113 (*)
│   │   │   │   │   │   ├── serde_path_to_error v0.1.15
│   │   │   │   │   │   │   ├── itoa v1.0.10
│   │   │   │   │   │   │   └── serde v1.0.196 (*)
│   │   │   │   │   │   ├── sha2 v0.10.8 (*)
│   │   │   │   │   │   ├── thiserror v1.0.57
│   │   │   │   │   │   │   └── thiserror-impl v1.0.57 (proc-macro)
│   │   │   │   │   │   │       ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   │   │       ├── quote v1.0.35 (*)
│   │   │   │   │   │   │       └── syn v2.0.49 (*)
│   │   │   │   │   │   └── url v2.5.0 (*)
│   │   │   │   │   ├── p256 v0.13.2
│   │   │   │   │   │   ├── ecdsa v0.16.9
│   │   │   │   │   │   │   ├── der v0.7.8 (*)
│   │   │   │   │   │   │   ├── digest v0.10.7 (*)
│   │   │   │   │   │   │   ├── elliptic-curve v0.13.8
│   │   │   │   │   │   │   │   ├── base16ct v0.2.0
│   │   │   │   │   │   │   │   ├── crypto-bigint v0.5.5
│   │   │   │   │   │   │   │   │   ├── generic-array v0.14.7 (*)
│   │   │   │   │   │   │   │   │   ├── rand_core v0.6.4 (*)
│   │   │   │   │   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   │   │   │   ├── digest v0.10.7 (*)
│   │   │   │   │   │   │   │   ├── ff v0.13.0
│   │   │   │   │   │   │   │   │   ├── rand_core v0.6.4 (*)
│   │   │   │   │   │   │   │   │   └── subtle v2.5.0
│   │   │   │   │   │   │   │   ├── generic-array v0.14.7 (*)
│   │   │   │   │   │   │   │   ├── group v0.13.0
│   │   │   │   │   │   │   │   │   ├── ff v0.13.0 (*)
│   │   │   │   │   │   │   │   │   ├── rand_core v0.6.4 (*)
│   │   │   │   │   │   │   │   │   └── subtle v2.5.0
│   │   │   │   │   │   │   │   ├── hkdf v0.12.4
│   │   │   │   │   │   │   │   │   └── hmac v0.12.1 (*)
│   │   │   │   │   │   │   │   ├── pem-rfc7468 v0.7.0 (*)
│   │   │   │   │   │   │   │   ├── pkcs8 v0.10.2 (*)
│   │   │   │   │   │   │   │   ├── rand_core v0.6.4 (*)
│   │   │   │   │   │   │   │   ├── sec1 v0.7.3
│   │   │   │   │   │   │   │   │   ├── base16ct v0.2.0
│   │   │   │   │   │   │   │   │   ├── der v0.7.8 (*)
│   │   │   │   │   │   │   │   │   ├── generic-array v0.14.7 (*)
│   │   │   │   │   │   │   │   │   ├── pkcs8 v0.10.2 (*)
│   │   │   │   │   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   │   │   ├── rfc6979 v0.4.0
│   │   │   │   │   │   │   │   ├── hmac v0.12.1 (*)
│   │   │   │   │   │   │   │   └── subtle v2.5.0
│   │   │   │   │   │   │   ├── signature v2.2.0 (*)
│   │   │   │   │   │   │   └── spki v0.7.3 (*)
│   │   │   │   │   │   ├── elliptic-curve v0.13.8 (*)
│   │   │   │   │   │   ├── primeorder v0.13.6
│   │   │   │   │   │   │   └── elliptic-curve v0.13.8 (*)
│   │   │   │   │   │   └── sha2 v0.10.8 (*)
│   │   │   │   │   ├── p384 v0.13.0
│   │   │   │   │   │   ├── ecdsa v0.16.9 (*)
│   │   │   │   │   │   ├── elliptic-curve v0.13.8 (*)
│   │   │   │   │   │   ├── primeorder v0.13.6 (*)
│   │   │   │   │   │   └── sha2 v0.10.8 (*)
│   │   │   │   │   ├── rand v0.8.5 (*)
│   │   │   │   │   ├── rsa v0.9.6
│   │   │   │   │   │   ├── const-oid v0.9.6
│   │   │   │   │   │   ├── digest v0.10.7 (*)
│   │   │   │   │   │   ├── num-bigint-dig v0.8.4
│   │   │   │   │   │   │   ├── byteorder v1.5.0
│   │   │   │   │   │   │   ├── lazy_static v1.4.0 (*)
│   │   │   │   │   │   │   ├── libm v0.2.8
│   │   │   │   │   │   │   ├── num-integer v0.1.46
│   │   │   │   │   │   │   │   └── num-traits v0.2.18 (*)
│   │   │   │   │   │   │   ├── num-iter v0.1.44
│   │   │   │   │   │   │   │   ├── num-integer v0.1.46 (*)
│   │   │   │   │   │   │   │   └── num-traits v0.2.18 (*)
│   │   │   │   │   │   │   │   [build-dependencies]
│   │   │   │   │   │   │   │   └── autocfg v1.1.0
│   │   │   │   │   │   │   ├── num-traits v0.2.18 (*)
│   │   │   │   │   │   │   ├── rand v0.8.5 (*)
│   │   │   │   │   │   │   ├── smallvec v1.13.1 (*)
│   │   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   │   ├── num-integer v0.1.46 (*)
│   │   │   │   │   │   ├── num-traits v0.2.18 (*)
│   │   │   │   │   │   ├── pkcs1 v0.7.5
│   │   │   │   │   │   │   ├── der v0.7.8 (*)
│   │   │   │   │   │   │   ├── pkcs8 v0.10.2 (*)
│   │   │   │   │   │   │   └── spki v0.7.3 (*)
│   │   │   │   │   │   ├── pkcs8 v0.10.2 (*)
│   │   │   │   │   │   ├── rand_core v0.6.4 (*)
│   │   │   │   │   │   ├── signature v2.2.0 (*)
│   │   │   │   │   │   ├── spki v0.7.3 (*)
│   │   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   ├── serde-value v0.7.0
│   │   │   │   │   │   ├── ordered-float v2.10.1
│   │   │   │   │   │   │   └── num-traits v0.2.18 (*)
│   │   │   │   │   │   └── serde v1.0.196 (*)
│   │   │   │   │   ├── serde_derive v1.0.196 (proc-macro) (*)
│   │   │   │   │   ├── serde_json v1.0.113 (*)
│   │   │   │   │   ├── serde_path_to_error v0.1.15 (*)
│   │   │   │   │   ├── serde_plain v1.0.2
│   │   │   │   │   │   └── serde v1.0.196 (*)
│   │   │   │   │   ├── serde_with v3.6.1
│   │   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   │   ├── serde_derive v1.0.196 (proc-macro) (*)
│   │   │   │   │   │   └── serde_with_macros v3.6.1 (proc-macro)
│   │   │   │   │   │       ├── darling v0.20.6
│   │   │   │   │   │       │   ├── darling_core v0.20.6
│   │   │   │   │   │       │   │   ├── fnv v1.0.7
│   │   │   │   │   │       │   │   ├── ident_case v1.0.1
│   │   │   │   │   │       │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   │       │   │   ├── quote v1.0.35 (*)
│   │   │   │   │   │       │   │   ├── strsim v0.10.0
│   │   │   │   │   │       │   │   └── syn v2.0.49 (*)
│   │   │   │   │   │       │   └── darling_macro v0.20.6 (proc-macro)
│   │   │   │   │   │       │       ├── darling_core v0.20.6 (*)
│   │   │   │   │   │       │       ├── quote v1.0.35 (*)
│   │   │   │   │   │       │       └── syn v2.0.49 (*)
│   │   │   │   │   │       ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   │       ├── quote v1.0.35 (*)
│   │   │   │   │   │       └── syn v2.0.49 (*)
│   │   │   │   │   ├── sha2 v0.10.8 (*)
│   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   ├── thiserror v1.0.57 (*)
│   │   │   │   │   └── url v2.5.0 (*)
│   │   │   │   ├── owo-colors v4.0.0
│   │   │   │   ├── phonenumber v0.3.3+8.13.9
│   │   │   │   │   ├── bincode v1.3.3
│   │   │   │   │   │   └── serde v1.0.196 (*)
│   │   │   │   │   ├── either v1.10.0
│   │   │   │   │   ├── fnv v1.0.7
│   │   │   │   │   ├── itertools v0.11.0
│   │   │   │   │   │   └── either v1.10.0
│   │   │   │   │   ├── lazy_static v1.4.0 (*)
│   │   │   │   │   ├── nom v7.1.3 (*)
│   │   │   │   │   ├── quick-xml v0.28.2
│   │   │   │   │   │   └── memchr v2.7.1
│   │   │   │   │   ├── regex v1.10.3 (*)
│   │   │   │   │   ├── regex-cache v0.2.1
│   │   │   │   │   │   ├── lru-cache v0.1.2
│   │   │   │   │   │   │   └── linked-hash-map v0.5.6
│   │   │   │   │   │   ├── oncemutex v0.1.1
│   │   │   │   │   │   ├── regex v1.10.3 (*)
│   │   │   │   │   │   └── regex-syntax v0.6.29
│   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   ├── serde_derive v1.0.196 (proc-macro) (*)
│   │   │   │   │   ├── strum v0.24.1
│   │   │   │   │   │   └── strum_macros v0.24.3 (proc-macro)
│   │   │   │   │   │       ├── heck v0.4.1
│   │   │   │   │   │       ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   │       ├── quote v1.0.35 (*)
│   │   │   │   │   │       ├── rustversion v1.0.14 (proc-macro)
│   │   │   │   │   │       └── syn v1.0.109 (*)
│   │   │   │   │   └── thiserror v1.0.57 (*)
│   │   │   │   │   [build-dependencies]
│   │   │   │   │   ├── bincode v1.3.3 (*)
│   │   │   │   │   ├── quick-xml v0.28.2 (*)
│   │   │   │   │   ├── regex v1.10.3 (*)
│   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   ├── serde_derive v1.0.196 (proc-macro) (*)
│   │   │   │   │   └── thiserror v1.0.57 (*)
│   │   │   │   ├── rustc-hash v1.1.0
│   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   ├── serde_path_to_error v0.1.15 (*)
│   │   │   │   └── url v2.5.0 (*)
│   │   │   ├── opentelemetry v0.21.0
│   │   │   │   ├── futures-core v0.3.30
│   │   │   │   ├── futures-sink v0.3.30
│   │   │   │   ├── indexmap v2.2.3 (*)
│   │   │   │   ├── once_cell v1.19.0
│   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   ├── thiserror v1.0.57 (*)
│   │   │   │   └── urlencoding v2.1.3
│   │   │   ├── pin-project-lite v0.2.13
│   │   │   ├── thiserror v1.0.57 (*)
│   │   │   └── tracing v0.1.40 (*)
│   │   ├── opentalk-db-storage v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/db-storage)
│   │   │   ├── anyhow v1.0.79
│   │   │   ├── barrel v0.7.0
│   │   │   ├── chrono v0.4.34 (*)
│   │   │   ├── chrono-tz v0.8.6
│   │   │   │   ├── chrono v0.4.34 (*)
│   │   │   │   ├── phf v0.11.2 (*)
│   │   │   │   └── serde v1.0.196 (*)
│   │   │   │   [build-dependencies]
│   │   │   │   └── chrono-tz-build v0.2.1
│   │   │   │       ├── parse-zoneinfo v0.3.0
│   │   │   │       │   └── regex v1.10.3 (*)
│   │   │   │       ├── phf v0.11.2
│   │   │   │       │   └── phf_shared v0.11.2
│   │   │   │       │       └── siphasher v0.3.11
│   │   │   │       └── phf_codegen v0.11.2
│   │   │   │           ├── phf_generator v0.11.2
│   │   │   │           │   ├── phf_shared v0.11.2 (*)
│   │   │   │           │   └── rand v0.8.5
│   │   │   │           │       └── rand_core v0.6.4
│   │   │   │           └── phf_shared v0.11.2 (*)
│   │   │   ├── derive_more v0.99.17 (proc-macro) (*)
│   │   │   ├── diesel v2.1.4 (*)
│   │   │   ├── diesel-async v0.3.2 (*)
│   │   │   ├── kustos v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos)
│   │   │   │   ├── actix-web v4.5.1 (*)
│   │   │   │   ├── anyhow v1.0.79
│   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   ├── barrel v0.7.0
│   │   │   │   ├── casbin v2.0.9 (https://github.com/kbalt/casbin-rs.git?rev=a5cd79704726b423030942769779ea7d4e8129ee#a5cd7970)
│   │   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   │   ├── fixedbitset v0.4.2
│   │   │   │   │   ├── once_cell v1.19.0
│   │   │   │   │   ├── parking_lot v0.12.1 (*)
│   │   │   │   │   ├── petgraph v0.6.4
│   │   │   │   │   │   ├── fixedbitset v0.4.2
│   │   │   │   │   │   └── indexmap v2.2.3 (*)
│   │   │   │   │   ├── regex v1.10.3 (*)
│   │   │   │   │   ├── rhai v1.17.1
│   │   │   │   │   │   ├── ahash v0.8.8 (*)
│   │   │   │   │   │   ├── bitflags v2.4.2
│   │   │   │   │   │   ├── num-traits v0.2.18 (*)
│   │   │   │   │   │   ├── once_cell v1.19.0
│   │   │   │   │   │   ├── rhai_codegen v2.0.0 (proc-macro)
│   │   │   │   │   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   │   │   ├── quote v1.0.35 (*)
│   │   │   │   │   │   │   └── syn v2.0.49 (*)
│   │   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   │   ├── smallvec v1.13.1 (*)
│   │   │   │   │   │   ├── smartstring v1.0.1
│   │   │   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   │   │   └── static_assertions v1.1.0
│   │   │   │   │   │   │   [build-dependencies]
│   │   │   │   │   │   │   ├── autocfg v1.1.0
│   │   │   │   │   │   │   └── version_check v0.9.4
│   │   │   │   │   │   └── thin-vec v0.2.13
│   │   │   │   │   │       └── serde v1.0.196 (*)
│   │   │   │   │   ├── ritelinked v0.3.2
│   │   │   │   │   │   ├── ahash v0.7.8
│   │   │   │   │   │   │   ├── getrandom v0.2.12 (*)
│   │   │   │   │   │   │   └── once_cell v1.19.0
│   │   │   │   │   │   │   [build-dependencies]
│   │   │   │   │   │   │   └── version_check v0.9.4
│   │   │   │   │   │   └── hashbrown v0.11.2
│   │   │   │   │   │       └── ahash v0.7.8 (*)
│   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   ├── thiserror v1.0.57 (*)
│   │   │   │   │   └── tokio v1.36.0 (*)
│   │   │   │   ├── diesel v2.1.4 (*)
│   │   │   │   ├── diesel-async v0.3.2 (*)
│   │   │   │   ├── futures v0.3.30
│   │   │   │   │   ├── futures-channel v0.3.30 (*)
│   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   ├── futures-executor v0.3.30
│   │   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   │   ├── futures-task v0.3.30
│   │   │   │   │   │   └── futures-util v0.3.30 (*)
│   │   │   │   │   ├── futures-io v0.3.30
│   │   │   │   │   ├── futures-sink v0.3.30
│   │   │   │   │   ├── futures-task v0.3.30
│   │   │   │   │   └── futures-util v0.3.30 (*)
│   │   │   │   ├── itertools v0.12.1
│   │   │   │   │   └── either v1.10.0
│   │   │   │   ├── kustos-shared v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos-shared)
│   │   │   │   │   ├── anyhow v1.0.79
│   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   ├── itertools v0.12.1 (*)
│   │   │   │   │   ├── thiserror v1.0.57 (*)
│   │   │   │   │   └── uuid v1.7.0 (*)
│   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   ├── opentalk-database v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/database) (*)
│   │   │   │   ├── opentelemetry v0.21.0 (*)
│   │   │   │   ├── parking_lot v0.12.1 (*)
│   │   │   │   ├── thiserror v1.0.57 (*)
│   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   ├── tracing v0.1.40 (*)
│   │   │   │   ├── tracing-futures v0.2.5
│   │   │   │   │   ├── pin-project v1.1.4
│   │   │   │   │   │   └── pin-project-internal v1.1.4 (proc-macro)
│   │   │   │   │   │       ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   │       ├── quote v1.0.35 (*)
│   │   │   │   │   │       └── syn v2.0.49 (*)
│   │   │   │   │   └── tracing v0.1.40 (*)
│   │   │   │   └── uuid v1.7.0 (*)
│   │   │   ├── log v0.4.20 (*)
│   │   │   ├── md5 v0.7.0
│   │   │   ├── opentalk-controller-settings v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-settings) (*)
│   │   │   ├── opentalk-database v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/database) (*)
│   │   │   ├── opentalk-diesel-newtype v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/diesel-newtype)
│   │   │   │   ├── diesel v2.1.4 (*)
│   │   │   │   ├── opentalk-diesel-newtype-impl v0.10.0 (proc-macro) (/home/silwol/Projects/opentalk/backend/services/controller/crates/diesel-newtype-impl)
│   │   │   │   │   ├── proc-macro-crate v3.1.0
│   │   │   │   │   │   └── toml_edit v0.21.1
│   │   │   │   │   │       ├── indexmap v2.2.3 (*)
│   │   │   │   │   │       ├── toml_datetime v0.6.5 (*)
│   │   │   │   │   │       └── winnow v0.5.40
│   │   │   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   ├── quote v1.0.35 (*)
│   │   │   │   │   └── syn v2.0.49 (*)
│   │   │   │   └── serde v1.0.196 (*)
│   │   │   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types)
│   │   │   │   ├── actix-http v3.6.0 (*)
│   │   │   │   ├── actix-web-httpauth v0.8.1
│   │   │   │   │   ├── actix-utils v3.0.1 (*)
│   │   │   │   │   ├── actix-web v4.5.1 (*)
│   │   │   │   │   ├── base64 v0.21.7
│   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   ├── futures-util v0.3.30 (*)
│   │   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   │   └── pin-project-lite v0.2.13
│   │   │   │   ├── base64 v0.21.7
│   │   │   │   ├── bincode v1.3.3 (*)
│   │   │   │   ├── bytes v1.5.0
│   │   │   │   ├── chrono v0.4.34 (*)
│   │   │   │   ├── chrono-tz v0.8.6 (*)
│   │   │   │   ├── clap v4.5.0
│   │   │   │   │   ├── clap_builder v4.5.0
│   │   │   │   │   │   ├── anstream v0.6.11 (*)
│   │   │   │   │   │   ├── anstyle v1.0.6
│   │   │   │   │   │   ├── clap_lex v0.7.0
│   │   │   │   │   │   └── strsim v0.11.0
│   │   │   │   │   └── clap_derive v4.5.0 (proc-macro)
│   │   │   │   │       ├── heck v0.4.1
│   │   │   │   │       ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │       ├── quote v1.0.35 (*)
│   │   │   │   │       └── syn v2.0.49 (*)
│   │   │   │   ├── derive_more v0.99.17 (proc-macro) (*)
│   │   │   │   ├── diesel v2.1.4 (*)
│   │   │   │   ├── email_address v0.2.4
│   │   │   │   │   └── serde v1.0.196 (*)
│   │   │   │   ├── kustos-shared v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos-shared) (*)
│   │   │   │   ├── opentalk-diesel-newtype v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/diesel-newtype) (*)
│   │   │   │   ├── opentalk-kustos-prefix v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos-prefix)
│   │   │   │   │   ├── kustos-shared v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos-shared) (*)
│   │   │   │   │   └── opentalk-kustos-prefix-impl v0.10.0 (proc-macro) (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos-prefix-impl)
│   │   │   │   │       ├── proc-macro-crate v3.1.0 (*)
│   │   │   │   │       ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │       ├── quote v1.0.35 (*)
│   │   │   │   │       └── syn v2.0.49 (*)
│   │   │   │   ├── rand v0.8.5 (*)
│   │   │   │   ├── redis v0.24.0
│   │   │   │   │   ├── arc-swap v1.6.0
│   │   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   ├── combine v4.6.6
│   │   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   │   ├── memchr v2.7.1
│   │   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   │   │   └── tokio-util v0.7.10 (*)
│   │   │   │   │   ├── futures v0.3.30 (*)
│   │   │   │   │   ├── futures-util v0.3.30 (*)
│   │   │   │   │   ├── itoa v1.0.10
│   │   │   │   │   ├── percent-encoding v2.3.1
│   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   ├── ryu v1.0.16
│   │   │   │   │   ├── sha1_smol v1.0.0
│   │   │   │   │   ├── socket2 v0.4.10
│   │   │   │   │   │   └── libc v0.2.153
│   │   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   │   ├── tokio-retry v0.3.0
│   │   │   │   │   │   ├── pin-project v1.1.4 (*)
│   │   │   │   │   │   ├── rand v0.8.5 (*)
│   │   │   │   │   │   └── tokio v1.36.0 (*)
│   │   │   │   │   ├── tokio-util v0.7.10 (*)
│   │   │   │   │   └── url v2.5.0 (*)
│   │   │   │   ├── redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args)
│   │   │   │   │   ├── redis v0.24.0 (*)
│   │   │   │   │   ├── redis-args-impl v0.10.0 (proc-macro) (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args-impl)
│   │   │   │   │   │   ├── opentalk-proc-macro-fields-helper v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/proc-macro-fields-helper)
│   │   │   │   │   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   │   │   ├── quote v1.0.35 (*)
│   │   │   │   │   │   │   └── syn v2.0.49 (*)
│   │   │   │   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │   │   ├── quote v1.0.35 (*)
│   │   │   │   │   │   └── syn v2.0.49 (*)
│   │   │   │   │   └── serde_json v1.0.113 (*)
│   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   ├── serde_json v1.0.113 (*)
│   │   │   │   ├── strum v0.26.1
│   │   │   │   │   └── strum_macros v0.26.1 (proc-macro)
│   │   │   │   │       ├── heck v0.4.1
│   │   │   │   │       ├── proc-macro2 v1.0.78 (*)
│   │   │   │   │       ├── quote v1.0.35 (*)
│   │   │   │   │       ├── rustversion v1.0.14 (proc-macro)
│   │   │   │   │       └── syn v2.0.49 (*)
│   │   │   │   ├── thiserror v1.0.57 (*)
│   │   │   │   ├── url v2.5.0 (*)
│   │   │   │   ├── uuid v1.7.0 (*)
│   │   │   │   └── validator v0.16.1
│   │   │   │       ├── idna v0.4.0
│   │   │   │       │   ├── unicode-bidi v0.3.15
│   │   │   │       │   └── unicode-normalization v0.1.22 (*)
│   │   │   │       ├── lazy_static v1.4.0 (*)
│   │   │   │       ├── regex v1.10.3 (*)
│   │   │   │       ├── serde v1.0.196 (*)
│   │   │   │       ├── serde_derive v1.0.196 (proc-macro) (*)
│   │   │   │       ├── serde_json v1.0.113 (*)
│   │   │   │       ├── url v2.5.0 (*)
│   │   │   │       └── validator_derive v0.16.0 (proc-macro)
│   │   │   │           ├── if_chain v1.0.2
│   │   │   │           ├── lazy_static v1.4.0
│   │   │   │           ├── proc-macro-error v1.0.4
│   │   │   │           │   ├── proc-macro-error-attr v1.0.4 (proc-macro)
│   │   │   │           │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │           │   │   └── quote v1.0.35 (*)
│   │   │   │           │   │   [build-dependencies]
│   │   │   │           │   │   └── version_check v0.9.4
│   │   │   │           │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │           │   ├── quote v1.0.35 (*)
│   │   │   │           │   └── syn v1.0.109 (*)
│   │   │   │           │   [build-dependencies]
│   │   │   │           │   └── version_check v0.9.4
│   │   │   │           ├── proc-macro2 v1.0.78 (*)
│   │   │   │           ├── quote v1.0.35 (*)
│   │   │   │           ├── regex v1.10.3 (*)
│   │   │   │           ├── syn v1.0.109 (*)
│   │   │   │           └── validator_types v0.16.0
│   │   │   │               ├── proc-macro2 v1.0.78 (*)
│   │   │   │               └── syn v1.0.109 (*)
│   │   │   ├── redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args) (*)
│   │   │   ├── refinery v0.8.12
│   │   │   │   ├── refinery-core v0.8.12
│   │   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   │   ├── cfg-if v1.0.0
│   │   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   │   ├── regex v1.10.3 (*)
│   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   ├── siphasher v1.0.0
│   │   │   │   │   ├── thiserror v1.0.57 (*)
│   │   │   │   │   ├── time v0.3.34 (*)
│   │   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   │   ├── tokio-postgres v0.7.10 (*)
│   │   │   │   │   ├── toml v0.8.10 (*)
│   │   │   │   │   ├── url v2.5.0 (*)
│   │   │   │   │   └── walkdir v2.4.0
│   │   │   │   │       └── same-file v1.0.6
│   │   │   │   └── refinery-macros v0.8.12 (proc-macro)
│   │   │   │       ├── proc-macro2 v1.0.78 (*)
│   │   │   │       ├── quote v1.0.35 (*)
│   │   │   │       ├── refinery-core v0.8.12
│   │   │   │       │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │       │   ├── cfg-if v1.0.0
│   │   │   │       │   ├── log v0.4.20
│   │   │   │       │   ├── regex v1.10.3 (*)
│   │   │   │       │   ├── serde v1.0.196 (*)
│   │   │   │       │   ├── siphasher v1.0.0
│   │   │   │       │   ├── thiserror v1.0.57 (*)
│   │   │   │       │   ├── time v0.3.34
│   │   │   │       │   │   ├── deranged v0.3.11 (*)
│   │   │   │       │   │   ├── itoa v1.0.10
│   │   │   │       │   │   ├── libc v0.2.153
│   │   │   │       │   │   ├── num-conv v0.1.0
│   │   │   │       │   │   ├── num_threads v0.1.7
│   │   │   │       │   │   ├── powerfmt v0.2.0
│   │   │   │       │   │   └── time-core v0.1.2
│   │   │   │       │   ├── toml v0.8.10 (*)
│   │   │   │       │   ├── url v2.5.0
│   │   │   │       │   │   ├── form_urlencoded v1.2.1 (*)
│   │   │   │       │   │   ├── idna v0.5.0 (*)
│   │   │   │       │   │   └── percent-encoding v2.3.1
│   │   │   │       │   └── walkdir v2.4.0 (*)
│   │   │   │       ├── regex v1.10.3 (*)
│   │   │   │       └── syn v2.0.49 (*)
│   │   │   ├── refinery-core v0.8.12 (*)
│   │   │   ├── rustc-hash v1.1.0
│   │   │   ├── serde v1.0.196 (*)
│   │   │   ├── serde_json v1.0.113 (*)
│   │   │   ├── strum v0.26.1 (*)
│   │   │   ├── thiserror v1.0.57 (*)
│   │   │   ├── tokio v1.36.0 (*)
│   │   │   ├── tracing v0.1.40 (*)
│   │   │   └── uuid v1.7.0 (*)
│   │   ├── opentalk-r3dlock v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/r3dlock)
│   │   │   ├── displaydoc v0.2.4 (proc-macro)
│   │   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │   ├── quote v1.0.35 (*)
│   │   │   │   └── syn v2.0.49 (*)
│   │   │   ├── rand v0.8.5 (*)
│   │   │   ├── redis v0.24.0 (*)
│   │   │   ├── thiserror v1.0.57 (*)
│   │   │   └── tokio v1.36.0 (*)
│   │   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core)
│   │   │   ├── actix-http v3.6.0 (*)
│   │   │   ├── anyhow v1.0.79
│   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   ├── aws-sdk-s3 v1.15.0
│   │   │   │   ├── aws-credential-types v1.1.5
│   │   │   │   │   ├── aws-smithy-async v1.1.6
│   │   │   │   │   │   ├── futures-util v0.3.30 (*)
│   │   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   │   └── tokio v1.36.0 (*)
│   │   │   │   │   ├── aws-smithy-runtime-api v1.1.6
│   │   │   │   │   │   ├── aws-smithy-async v1.1.6 (*)
│   │   │   │   │   │   ├── aws-smithy-types v1.1.6
│   │   │   │   │   │   │   ├── base64-simd v0.8.0
│   │   │   │   │   │   │   │   ├── outref v0.5.1
│   │   │   │   │   │   │   │   └── vsimd v0.8.0
│   │   │   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   │   │   ├── bytes-utils v0.1.4
│   │   │   │   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   │   │   │   └── either v1.10.0
│   │   │   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   │   │   ├── http-body v0.4.6 (*)
│   │   │   │   │   │   │   ├── itoa v1.0.10
│   │   │   │   │   │   │   ├── num-integer v0.1.46 (*)
│   │   │   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   │   │   ├── pin-utils v0.1.0
│   │   │   │   │   │   │   ├── ryu v1.0.16
│   │   │   │   │   │   │   ├── time v0.3.34 (*)
│   │   │   │   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   │   │   │   └── tokio-util v0.7.10 (*)
│   │   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   │   ├── http v1.0.0
│   │   │   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   │   │   ├── fnv v1.0.7
│   │   │   │   │   │   │   └── itoa v1.0.10
│   │   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   │   │   └── tracing v0.1.40 (*)
│   │   │   │   │   ├── aws-smithy-types v1.1.6 (*)
│   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   ├── aws-runtime v1.1.5
│   │   │   │   │   ├── aws-credential-types v1.1.5 (*)
│   │   │   │   │   ├── aws-sigv4 v1.1.5
│   │   │   │   │   │   ├── aws-credential-types v1.1.5 (*)
│   │   │   │   │   │   ├── aws-smithy-eventstream v0.60.4
│   │   │   │   │   │   │   ├── aws-smithy-types v1.1.6 (*)
│   │   │   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   │   │   └── crc32fast v1.4.0 (*)
│   │   │   │   │   │   ├── aws-smithy-http v0.60.5
│   │   │   │   │   │   │   ├── aws-smithy-eventstream v0.60.4 (*)
│   │   │   │   │   │   │   ├── aws-smithy-runtime-api v1.1.6 (*)
│   │   │   │   │   │   │   ├── aws-smithy-types v1.1.6 (*)
│   │   │   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   │   │   ├── bytes-utils v0.1.4 (*)
│   │   │   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   │   │   ├── http-body v0.4.6 (*)
│   │   │   │   │   │   │   ├── once_cell v1.19.0
│   │   │   │   │   │   │   ├── percent-encoding v2.3.1
│   │   │   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   │   │   ├── pin-utils v0.1.0
│   │   │   │   │   │   │   └── tracing v0.1.40 (*)
│   │   │   │   │   │   ├── aws-smithy-runtime-api v1.1.6 (*)
│   │   │   │   │   │   ├── aws-smithy-types v1.1.6 (*)
│   │   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   │   ├── crypto-bigint v0.5.5 (*)
│   │   │   │   │   │   ├── form_urlencoded v1.2.1 (*)
│   │   │   │   │   │   ├── hex v0.4.3
│   │   │   │   │   │   ├── hmac v0.12.1 (*)
│   │   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   │   ├── http v1.0.0 (*)
│   │   │   │   │   │   ├── once_cell v1.19.0
│   │   │   │   │   │   ├── p256 v0.11.1
│   │   │   │   │   │   │   ├── ecdsa v0.14.8
│   │   │   │   │   │   │   │   ├── der v0.6.1
│   │   │   │   │   │   │   │   │   ├── const-oid v0.9.6
│   │   │   │   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   │   │   │   ├── elliptic-curve v0.12.3
│   │   │   │   │   │   │   │   │   ├── base16ct v0.1.1
│   │   │   │   │   │   │   │   │   ├── crypto-bigint v0.4.9
│   │   │   │   │   │   │   │   │   │   ├── generic-array v0.14.7 (*)
│   │   │   │   │   │   │   │   │   │   ├── rand_core v0.6.4 (*)
│   │   │   │   │   │   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   │   │   │   │   ├── der v0.6.1 (*)
│   │   │   │   │   │   │   │   │   ├── digest v0.10.7 (*)
│   │   │   │   │   │   │   │   │   ├── ff v0.12.1
│   │   │   │   │   │   │   │   │   │   ├── rand_core v0.6.4 (*)
│   │   │   │   │   │   │   │   │   │   └── subtle v2.5.0
│   │   │   │   │   │   │   │   │   ├── generic-array v0.14.7 (*)
│   │   │   │   │   │   │   │   │   ├── group v0.12.1
│   │   │   │   │   │   │   │   │   │   ├── ff v0.12.1 (*)
│   │   │   │   │   │   │   │   │   │   ├── rand_core v0.6.4 (*)
│   │   │   │   │   │   │   │   │   │   └── subtle v2.5.0
│   │   │   │   │   │   │   │   │   ├── pkcs8 v0.9.0
│   │   │   │   │   │   │   │   │   │   ├── der v0.6.1 (*)
│   │   │   │   │   │   │   │   │   │   └── spki v0.6.0
│   │   │   │   │   │   │   │   │   │       ├── base64ct v1.6.0
│   │   │   │   │   │   │   │   │   │       └── der v0.6.1 (*)
│   │   │   │   │   │   │   │   │   ├── rand_core v0.6.4 (*)
│   │   │   │   │   │   │   │   │   ├── sec1 v0.3.0
│   │   │   │   │   │   │   │   │   │   ├── base16ct v0.1.1
│   │   │   │   │   │   │   │   │   │   ├── der v0.6.1 (*)
│   │   │   │   │   │   │   │   │   │   ├── generic-array v0.14.7 (*)
│   │   │   │   │   │   │   │   │   │   ├── pkcs8 v0.9.0 (*)
│   │   │   │   │   │   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   │   │   │   ├── rfc6979 v0.3.1
│   │   │   │   │   │   │   │   │   ├── crypto-bigint v0.4.9 (*)
│   │   │   │   │   │   │   │   │   ├── hmac v0.12.1 (*)
│   │   │   │   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   │   │   │   └── signature v1.6.4
│   │   │   │   │   │   │   │       ├── digest v0.10.7 (*)
│   │   │   │   │   │   │   │       └── rand_core v0.6.4 (*)
│   │   │   │   │   │   │   ├── elliptic-curve v0.12.3 (*)
│   │   │   │   │   │   │   └── sha2 v0.10.8 (*)
│   │   │   │   │   │   ├── percent-encoding v2.3.1
│   │   │   │   │   │   ├── ring v0.17.7 (*)
│   │   │   │   │   │   ├── sha2 v0.10.8 (*)
│   │   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   │   ├── time v0.3.34 (*)
│   │   │   │   │   │   ├── tracing v0.1.40 (*)
│   │   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   │   ├── aws-smithy-async v1.1.6 (*)
│   │   │   │   │   ├── aws-smithy-eventstream v0.60.4 (*)
│   │   │   │   │   ├── aws-smithy-http v0.60.5 (*)
│   │   │   │   │   ├── aws-smithy-runtime-api v1.1.6 (*)
│   │   │   │   │   ├── aws-smithy-types v1.1.6 (*)
│   │   │   │   │   ├── aws-types v1.1.5
│   │   │   │   │   │   ├── aws-credential-types v1.1.5 (*)
│   │   │   │   │   │   ├── aws-smithy-async v1.1.6 (*)
│   │   │   │   │   │   ├── aws-smithy-runtime-api v1.1.6 (*)
│   │   │   │   │   │   ├── aws-smithy-types v1.1.6 (*)
│   │   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   │   └── tracing v0.1.40 (*)
│   │   │   │   │   │   [build-dependencies]
│   │   │   │   │   │   └── rustc_version v0.4.0 (*)
│   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   ├── fastrand v2.0.1
│   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   ├── http-body v0.4.6 (*)
│   │   │   │   │   ├── percent-encoding v2.3.1
│   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   ├── tracing v0.1.40 (*)
│   │   │   │   │   └── uuid v1.7.0 (*)
│   │   │   │   ├── aws-sigv4 v1.1.5 (*)
│   │   │   │   ├── aws-smithy-async v1.1.6 (*)
│   │   │   │   ├── aws-smithy-checksums v0.60.5
│   │   │   │   │   ├── aws-smithy-http v0.60.5 (*)
│   │   │   │   │   ├── aws-smithy-types v1.1.6 (*)
│   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   ├── crc32c v0.6.5
│   │   │   │   │   │   [build-dependencies]
│   │   │   │   │   │   └── rustc_version v0.4.0 (*)
│   │   │   │   │   ├── crc32fast v1.4.0 (*)
│   │   │   │   │   ├── hex v0.4.3
│   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   ├── http-body v0.4.6 (*)
│   │   │   │   │   ├── md-5 v0.10.6 (*)
│   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   ├── sha1 v0.10.6 (*)
│   │   │   │   │   ├── sha2 v0.10.8 (*)
│   │   │   │   │   └── tracing v0.1.40 (*)
│   │   │   │   ├── aws-smithy-eventstream v0.60.4 (*)
│   │   │   │   ├── aws-smithy-http v0.60.5 (*)
│   │   │   │   ├── aws-smithy-json v0.60.5
│   │   │   │   │   └── aws-smithy-types v1.1.6 (*)
│   │   │   │   ├── aws-smithy-runtime v1.1.6
│   │   │   │   │   ├── aws-smithy-async v1.1.6 (*)
│   │   │   │   │   ├── aws-smithy-http v0.60.5 (*)
│   │   │   │   │   ├── aws-smithy-runtime-api v1.1.6 (*)
│   │   │   │   │   ├── aws-smithy-types v1.1.6 (*)
│   │   │   │   │   ├── bytes v1.5.0
│   │   │   │   │   ├── fastrand v2.0.1
│   │   │   │   │   ├── h2 v0.3.24 (*)
│   │   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   │   ├── http-body v0.4.6 (*)
│   │   │   │   │   ├── hyper v0.14.28 (*)
│   │   │   │   │   ├── hyper-rustls v0.24.2 (*)
│   │   │   │   │   ├── once_cell v1.19.0
│   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   ├── pin-utils v0.1.0
│   │   │   │   │   ├── rustls v0.21.10 (*)
│   │   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   │   └── tracing v0.1.40 (*)
│   │   │   │   ├── aws-smithy-runtime-api v1.1.6 (*)
│   │   │   │   ├── aws-smithy-types v1.1.6 (*)
│   │   │   │   ├── aws-smithy-xml v0.60.5
│   │   │   │   │   └── xmlparser v0.13.6
│   │   │   │   ├── aws-types v1.1.5 (*)
│   │   │   │   ├── bytes v1.5.0
│   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   ├── http-body v0.4.6 (*)
│   │   │   │   ├── once_cell v1.19.0
│   │   │   │   ├── percent-encoding v2.3.1
│   │   │   │   ├── regex-lite v0.1.5
│   │   │   │   ├── tracing v0.1.40 (*)
│   │   │   │   └── url v2.5.0 (*)
│   │   │   ├── bytes v1.5.0
│   │   │   ├── bytestring v1.3.1 (*)
│   │   │   ├── futures v0.3.30 (*)
│   │   │   ├── kustos v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos) (*)
│   │   │   ├── lapin v2.3.1
│   │   │   │   ├── amq-protocol v7.1.2
│   │   │   │   │   ├── amq-protocol-tcp v7.1.2
│   │   │   │   │   │   ├── amq-protocol-uri v7.1.2
│   │   │   │   │   │   │   ├── amq-protocol-types v7.1.2
│   │   │   │   │   │   │   │   ├── cookie-factory v0.3.2
│   │   │   │   │   │   │   │   ├── nom v7.1.3 (*)
│   │   │   │   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   │   │   │   └── serde_json v1.0.113 (*)
│   │   │   │   │   │   │   ├── percent-encoding v2.3.1
│   │   │   │   │   │   │   └── url v2.5.0 (*)
│   │   │   │   │   │   ├── tcp-stream v0.26.1
│   │   │   │   │   │   │   └── cfg-if v1.0.0
│   │   │   │   │   │   └── tracing v0.1.40 (*)
│   │   │   │   │   ├── amq-protocol-types v7.1.2 (*)
│   │   │   │   │   ├── amq-protocol-uri v7.1.2 (*)
│   │   │   │   │   ├── cookie-factory v0.3.2
│   │   │   │   │   ├── nom v7.1.3 (*)
│   │   │   │   │   └── serde v1.0.196 (*)
│   │   │   │   ├── async-global-executor-trait v2.1.0
│   │   │   │   │   ├── async-global-executor v2.4.1
│   │   │   │   │   │   ├── async-channel v2.2.0
│   │   │   │   │   │   │   ├── concurrent-queue v2.4.0
│   │   │   │   │   │   │   │   └── crossbeam-utils v0.8.19
│   │   │   │   │   │   │   ├── event-listener v5.0.0
│   │   │   │   │   │   │   │   ├── concurrent-queue v2.4.0 (*)
│   │   │   │   │   │   │   │   ├── parking v2.2.0
│   │   │   │   │   │   │   │   └── pin-project-lite v0.2.13
│   │   │   │   │   │   │   ├── event-listener-strategy v0.5.0
│   │   │   │   │   │   │   │   ├── event-listener v5.0.0 (*)
│   │   │   │   │   │   │   │   └── pin-project-lite v0.2.13
│   │   │   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   │   │   └── pin-project-lite v0.2.13
│   │   │   │   │   │   ├── async-executor v1.8.0
│   │   │   │   │   │   │   ├── async-lock v3.3.0
│   │   │   │   │   │   │   │   ├── event-listener v4.0.3
│   │   │   │   │   │   │   │   │   ├── concurrent-queue v2.4.0 (*)
│   │   │   │   │   │   │   │   │   ├── parking v2.2.0
│   │   │   │   │   │   │   │   │   └── pin-project-lite v0.2.13
│   │   │   │   │   │   │   │   ├── event-listener-strategy v0.4.0
│   │   │   │   │   │   │   │   │   ├── event-listener v4.0.3 (*)
│   │   │   │   │   │   │   │   │   └── pin-project-lite v0.2.13
│   │   │   │   │   │   │   │   └── pin-project-lite v0.2.13
│   │   │   │   │   │   │   ├── async-task v4.7.0
│   │   │   │   │   │   │   ├── concurrent-queue v2.4.0 (*)
│   │   │   │   │   │   │   ├── fastrand v2.0.1
│   │   │   │   │   │   │   ├── futures-lite v2.2.0
│   │   │   │   │   │   │   │   ├── fastrand v2.0.1
│   │   │   │   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   │   │   │   ├── futures-io v0.3.30
│   │   │   │   │   │   │   │   ├── parking v2.2.0
│   │   │   │   │   │   │   │   └── pin-project-lite v0.2.13
│   │   │   │   │   │   │   └── slab v0.4.9 (*)
│   │   │   │   │   │   ├── async-io v2.3.1
│   │   │   │   │   │   │   ├── async-lock v3.3.0 (*)
│   │   │   │   │   │   │   ├── cfg-if v1.0.0
│   │   │   │   │   │   │   ├── concurrent-queue v2.4.0 (*)
│   │   │   │   │   │   │   ├── futures-io v0.3.30
│   │   │   │   │   │   │   ├── futures-lite v2.2.0 (*)
│   │   │   │   │   │   │   ├── parking v2.2.0
│   │   │   │   │   │   │   ├── polling v3.4.0
│   │   │   │   │   │   │   │   ├── cfg-if v1.0.0
│   │   │   │   │   │   │   │   ├── rustix v0.38.31
│   │   │   │   │   │   │   │   │   ├── bitflags v2.4.2
│   │   │   │   │   │   │   │   │   └── linux-raw-sys v0.4.13
│   │   │   │   │   │   │   │   └── tracing v0.1.40 (*)
│   │   │   │   │   │   │   ├── rustix v0.38.31 (*)
│   │   │   │   │   │   │   ├── slab v0.4.9 (*)
│   │   │   │   │   │   │   └── tracing v0.1.40 (*)
│   │   │   │   │   │   ├── async-lock v3.3.0 (*)
│   │   │   │   │   │   ├── blocking v1.5.1
│   │   │   │   │   │   │   ├── async-channel v2.2.0 (*)
│   │   │   │   │   │   │   ├── async-lock v3.3.0 (*)
│   │   │   │   │   │   │   ├── async-task v4.7.0
│   │   │   │   │   │   │   ├── fastrand v2.0.1
│   │   │   │   │   │   │   ├── futures-io v0.3.30
│   │   │   │   │   │   │   ├── futures-lite v2.2.0 (*)
│   │   │   │   │   │   │   ├── piper v0.2.1
│   │   │   │   │   │   │   │   ├── atomic-waker v1.1.2
│   │   │   │   │   │   │   │   ├── fastrand v2.0.1
│   │   │   │   │   │   │   │   └── futures-io v0.3.30
│   │   │   │   │   │   │   └── tracing v0.1.40 (*)
│   │   │   │   │   │   ├── futures-lite v2.2.0 (*)
│   │   │   │   │   │   └── once_cell v1.19.0
│   │   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   │   └── executor-trait v2.1.0
│   │   │   │   │       └── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   ├── async-reactor-trait v1.1.0
│   │   │   │   │   ├── async-io v1.13.0
│   │   │   │   │   │   ├── async-lock v2.8.0
│   │   │   │   │   │   │   └── event-listener v2.5.3
│   │   │   │   │   │   ├── cfg-if v1.0.0
│   │   │   │   │   │   ├── concurrent-queue v2.4.0 (*)
│   │   │   │   │   │   ├── futures-lite v1.13.0
│   │   │   │   │   │   │   ├── fastrand v1.9.0
│   │   │   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   │   │   ├── futures-io v0.3.30
│   │   │   │   │   │   │   ├── memchr v2.7.1
│   │   │   │   │   │   │   ├── parking v2.2.0
│   │   │   │   │   │   │   ├── pin-project-lite v0.2.13
│   │   │   │   │   │   │   └── waker-fn v1.1.1
│   │   │   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   │   │   ├── parking v2.2.0
│   │   │   │   │   │   ├── polling v2.8.0
│   │   │   │   │   │   │   ├── cfg-if v1.0.0
│   │   │   │   │   │   │   ├── libc v0.2.153
│   │   │   │   │   │   │   └── log v0.4.20 (*)
│   │   │   │   │   │   │   [build-dependencies]
│   │   │   │   │   │   │   └── autocfg v1.1.0
│   │   │   │   │   │   ├── rustix v0.37.27
│   │   │   │   │   │   │   ├── bitflags v1.3.2
│   │   │   │   │   │   │   ├── io-lifetimes v1.0.11
│   │   │   │   │   │   │   │   └── libc v0.2.153
│   │   │   │   │   │   │   └── linux-raw-sys v0.3.8
│   │   │   │   │   │   ├── slab v0.4.9 (*)
│   │   │   │   │   │   ├── socket2 v0.4.10 (*)
│   │   │   │   │   │   └── waker-fn v1.1.1
│   │   │   │   │   │   [build-dependencies]
│   │   │   │   │   │   └── autocfg v1.1.0
│   │   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   └── reactor-trait v1.1.0
│   │   │   │   │       ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   │       ├── futures-core v0.3.30
│   │   │   │   │       └── futures-io v0.3.30
│   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   ├── executor-trait v2.1.0 (*)
│   │   │   │   ├── flume v0.10.14
│   │   │   │   │   ├── futures-core v0.3.30
│   │   │   │   │   ├── futures-sink v0.3.30
│   │   │   │   │   ├── pin-project v1.1.4 (*)
│   │   │   │   │   └── spin v0.9.8 (*)
│   │   │   │   ├── futures-core v0.3.30
│   │   │   │   ├── futures-io v0.3.30
│   │   │   │   ├── parking_lot v0.12.1 (*)
│   │   │   │   ├── pinky-swear v6.2.0
│   │   │   │   │   ├── doc-comment v0.3.3
│   │   │   │   │   ├── flume v0.11.0
│   │   │   │   │   │   └── spin v0.9.8 (*)
│   │   │   │   │   ├── parking_lot v0.12.1 (*)
│   │   │   │   │   └── tracing v0.1.40 (*)
│   │   │   │   ├── reactor-trait v1.1.0 (*)
│   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   ├── tracing v0.1.40 (*)
│   │   │   │   └── waker-fn v1.1.1
│   │   │   ├── lapin-pool v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/lapin-pool)
│   │   │   │   ├── anyhow v1.0.79
│   │   │   │   ├── lapin v2.3.1 (*)
│   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   ├── tokio-executor-trait v2.1.1
│   │   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   │   ├── executor-trait v2.1.0 (*)
│   │   │   │   │   └── tokio v1.36.0 (*)
│   │   │   │   └── tokio-reactor-trait v1.1.0
│   │   │   │       ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │       ├── futures-core v0.3.30
│   │   │   │       ├── futures-io v0.3.30
│   │   │   │       ├── reactor-trait v1.1.0 (*)
│   │   │   │       ├── tokio v1.36.0 (*)
│   │   │   │       └── tokio-stream v0.1.14
│   │   │   │           ├── futures-core v0.3.30
│   │   │   │           ├── pin-project-lite v0.2.13
│   │   │   │           ├── tokio v1.36.0 (*)
│   │   │   │           └── tokio-util v0.7.10 (*)
│   │   │   ├── log v0.4.20 (*)
│   │   │   ├── opentalk-controller-settings v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-settings) (*)
│   │   │   ├── opentalk-database v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/database) (*)
│   │   │   ├── opentalk-db-storage v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/db-storage) (*)
│   │   │   ├── opentalk-r3dlock v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/r3dlock) (*)
│   │   │   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   │   │   ├── opentelemetry v0.21.0 (*)
│   │   │   ├── redis v0.24.0 (*)
│   │   │   ├── redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args) (*)
│   │   │   ├── rustc-hash v1.1.0
│   │   │   ├── serde v1.0.196 (*)
│   │   │   ├── serde_json v1.0.113 (*)
│   │   │   ├── slotmap v1.0.7
│   │   │   │   [build-dependencies]
│   │   │   │   └── version_check v0.9.4
│   │   │   ├── thiserror v1.0.57 (*)
│   │   │   ├── tokio v1.36.0 (*)
│   │   │   ├── tokio-stream v0.1.14 (*)
│   │   │   ├── tracing v0.1.40 (*)
│   │   │   ├── url v2.5.0 (*)
│   │   │   └── uuid v1.7.0 (*)
│   │   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   │   ├── redis v0.24.0 (*)
│   │   ├── redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args) (*)
│   │   ├── serde_json v1.0.113 (*)
│   │   ├── tokio v1.36.0 (*)
│   │   ├── tracing v0.1.40 (*)
│   │   └── uuid v1.7.0 (*)
│   ├── opentalk-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/core)
│   │   ├── anyhow v1.0.79
│   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│   │   └── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   ├── opentalk-integration v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/integration)
│   │   ├── anyhow v1.0.79
│   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│   │   └── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   ├── opentalk-janus-media v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/janus-media)
│   │   ├── anyhow v1.0.79
│   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   ├── futures v0.3.30 (*)
│   │   ├── lapin-pool v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/lapin-pool) (*)
│   │   ├── log v0.4.20 (*)
│   │   ├── opentalk-controller-settings v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-settings) (*)
│   │   ├── opentalk-janus-client v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/janus-client)
│   │   │   ├── displaydoc v0.2.4 (proc-macro) (*)
│   │   │   ├── futures v0.3.30 (*)
│   │   │   ├── lapin v2.3.1 (*)
│   │   │   ├── log v0.4.20 (*)
│   │   │   ├── parking_lot v0.12.1 (*)
│   │   │   ├── rand v0.8.5 (*)
│   │   │   ├── serde v1.0.196 (*)
│   │   │   ├── serde_json v1.0.113 (*)
│   │   │   ├── serde_repr v0.1.18 (proc-macro)
│   │   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   │   ├── quote v1.0.35 (*)
│   │   │   │   └── syn v2.0.49 (*)
│   │   │   ├── thiserror v1.0.57 (*)
│   │   │   ├── tokio v1.36.0 (*)
│   │   │   ├── tokio-tungstenite v0.21.0
│   │   │   │   ├── futures-util v0.3.30 (*)
│   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   ├── rustls v0.22.2
│   │   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   │   ├── ring v0.17.7 (*)
│   │   │   │   │   ├── rustls-pki-types v1.3.0
│   │   │   │   │   ├── rustls-webpki v0.102.2
│   │   │   │   │   │   ├── ring v0.17.7 (*)
│   │   │   │   │   │   ├── rustls-pki-types v1.3.0
│   │   │   │   │   │   └── untrusted v0.9.0
│   │   │   │   │   ├── subtle v2.5.0
│   │   │   │   │   └── zeroize v1.7.0
│   │   │   │   ├── rustls-native-certs v0.7.0
│   │   │   │   │   ├── openssl-probe v0.1.5
│   │   │   │   │   ├── rustls-pemfile v2.1.0
│   │   │   │   │   │   ├── base64 v0.21.7
│   │   │   │   │   │   └── rustls-pki-types v1.3.0
│   │   │   │   │   └── rustls-pki-types v1.3.0
│   │   │   │   ├── rustls-pki-types v1.3.0
│   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   ├── tokio-rustls v0.25.0
│   │   │   │   │   ├── rustls v0.22.2 (*)
│   │   │   │   │   ├── rustls-pki-types v1.3.0
│   │   │   │   │   └── tokio v1.36.0 (*)
│   │   │   │   └── tungstenite v0.21.0
│   │   │   │       ├── byteorder v1.5.0
│   │   │   │       ├── bytes v1.5.0
│   │   │   │       ├── data-encoding v2.5.0
│   │   │   │       ├── http v1.0.0 (*)
│   │   │   │       ├── httparse v1.8.0
│   │   │   │       ├── log v0.4.20 (*)
│   │   │   │       ├── rand v0.8.5 (*)
│   │   │   │       ├── rustls v0.22.2 (*)
│   │   │   │       ├── rustls-pki-types v1.3.0
│   │   │   │       ├── sha1 v0.10.6 (*)
│   │   │   │       ├── thiserror v1.0.57 (*)
│   │   │   │       ├── url v2.5.0 (*)
│   │   │   │       └── utf-8 v0.7.6
│   │   │   └── tracing v0.1.40 (*)
│   │   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│   │   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   │   ├── pin-project-lite v0.2.13
│   │   ├── redis v0.24.0 (*)
│   │   ├── redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args) (*)
│   │   ├── serde v1.0.196 (*)
│   │   ├── serde_json v1.0.113 (*)
│   │   ├── tokio v1.36.0 (*)
│   │   ├── tokio-stream v0.1.14 (*)
│   │   └── tracing v0.1.40 (*)
│   ├── opentalk-polls v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/polls)
│   │   ├── anyhow v1.0.79
│   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   ├── futures v0.3.30 (*)
│   │   ├── log v0.4.20 (*)
│   │   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│   │   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   │   ├── redis v0.24.0 (*)
│   │   ├── redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args) (*)
│   │   ├── serde v1.0.196 (*)
│   │   ├── tokio v1.36.0 (*)
│   │   └── tracing v0.1.40 (*)
│   ├── opentalk-protocol v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/protocol)
│   │   ├── anyhow v1.0.79
│   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   ├── chrono v0.4.34 (*)
│   │   ├── futures v0.3.30 (*)
│   │   ├── log v0.4.20 (*)
│   │   ├── opentalk-controller-settings v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-settings) (*)
│   │   ├── opentalk-database v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/database) (*)
│   │   ├── opentalk-etherpad-client v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/etherpad-client)
│   │   │   ├── anyhow v1.0.79
│   │   │   ├── bytes v1.5.0
│   │   │   ├── futures v0.3.30 (*)
│   │   │   ├── reqwest v0.11.24 (*)
│   │   │   ├── serde v1.0.196 (*)
│   │   │   ├── serde_json v1.0.113 (*)
│   │   │   └── serde_repr v0.1.18 (proc-macro) (*)
│   │   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│   │   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   │   ├── redis v0.24.0 (*)
│   │   ├── redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args) (*)
│   │   ├── serde v1.0.196 (*)
│   │   └── tracing v0.1.40 (*)
│   ├── opentalk-shared-folder v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/shared-folder)
│   │   ├── anyhow v1.0.79
│   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   ├── log v0.4.20 (*)
│   │   ├── opentalk-database v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/database) (*)
│   │   ├── opentalk-db-storage v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/db-storage) (*)
│   │   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│   │   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   │   ├── redis v0.24.0 (*)
│   │   ├── redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args) (*)
│   │   └── tracing v0.1.40 (*)
│   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│   ├── opentalk-timer v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/timer)
│   │   ├── anyhow v1.0.79
│   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   ├── chrono v0.4.34 (*)
│   │   ├── futures v0.3.30 (*)
│   │   ├── log v0.4.20 (*)
│   │   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│   │   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   │   ├── redis v0.24.0 (*)
│   │   ├── redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args) (*)
│   │   ├── serde v1.0.196 (*)
│   │   ├── tokio v1.36.0 (*)
│   │   ├── tracing v0.1.40 (*)
│   │   └── uuid v1.7.0 (*)
│   └── opentalk-whiteboard v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/whiteboard)
│       ├── anyhow v1.0.79
│       ├── async-trait v0.1.77 (proc-macro) (*)
│       ├── bytes v1.5.0
│       ├── futures v0.3.30 (*)
│       ├── log v0.4.20 (*)
│       ├── opentalk-controller-settings v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-settings) (*)
│       ├── opentalk-database v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/database) (*)
│       ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│       ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│       ├── redis v0.24.0 (*)
│       ├── redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args) (*)
│       ├── reqwest v0.11.24 (*)
│       ├── serde v1.0.196 (*)
│       ├── tracing v0.1.40 (*)
│       └── url v2.5.0 (*)
├── opentalk-controller-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-core)
│   ├── actix v0.13.3
│   │   ├── actix-macros v0.2.4 (proc-macro) (*)
│   │   ├── actix-rt v2.9.0 (*)
│   │   ├── actix_derive v0.6.1 (proc-macro)
│   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   ├── quote v1.0.35 (*)
│   │   │   └── syn v2.0.49 (*)
│   │   ├── bitflags v2.4.2
│   │   ├── bytes v1.5.0
│   │   ├── crossbeam-channel v0.5.11
│   │   │   └── crossbeam-utils v0.8.19
│   │   ├── futures-core v0.3.30
│   │   ├── futures-sink v0.3.30
│   │   ├── futures-task v0.3.30
│   │   ├── futures-util v0.3.30 (*)
│   │   ├── log v0.4.20 (*)
│   │   ├── once_cell v1.19.0
│   │   ├── parking_lot v0.12.1 (*)
│   │   ├── pin-project-lite v0.2.13
│   │   ├── smallvec v1.13.1 (*)
│   │   ├── tokio v1.36.0 (*)
│   │   └── tokio-util v0.7.10 (*)
│   ├── actix-cors v0.7.0
│   │   ├── actix-utils v3.0.1 (*)
│   │   ├── actix-web v4.5.1 (*)
│   │   ├── derive_more v0.99.17 (proc-macro) (*)
│   │   ├── futures-util v0.3.30 (*)
│   │   ├── log v0.4.20 (*)
│   │   ├── once_cell v1.19.0
│   │   └── smallvec v1.13.1 (*)
│   ├── actix-http v3.6.0 (*)
│   ├── actix-rt v2.9.0 (*)
│   ├── actix-web v4.5.1 (*)
│   ├── actix-web-actors v4.3.0
│   │   ├── actix v0.13.3 (*)
│   │   ├── actix-codec v0.5.2 (*)
│   │   ├── actix-http v3.6.0 (*)
│   │   ├── actix-web v4.5.1 (*)
│   │   ├── bytes v1.5.0
│   │   ├── bytestring v1.3.1 (*)
│   │   ├── futures-core v0.3.30
│   │   ├── pin-project-lite v0.2.13
│   │   ├── tokio v1.36.0 (*)
│   │   └── tokio-util v0.7.10 (*)
│   ├── actix-web-httpauth v0.8.1 (*)
│   ├── anstream v0.6.11 (*)
│   ├── anyhow v1.0.79
│   ├── arc-swap v1.6.0
│   ├── async-trait v0.1.77 (proc-macro) (*)
│   ├── base64 v0.21.7
│   ├── bytes v1.5.0
│   ├── bytestring v1.3.1 (*)
│   ├── chrono v0.4.34 (*)
│   ├── chrono-tz v0.8.6 (*)
│   ├── clap v4.5.0 (*)
│   ├── diesel v2.1.4 (*)
│   ├── diesel-async v0.3.2 (*)
│   ├── either v1.10.0
│   ├── email_address v0.2.4 (*)
│   ├── futures v0.3.30 (*)
│   ├── itertools v0.12.1 (*)
│   ├── jsonwebtoken v9.2.0
│   │   ├── base64 v0.21.7
│   │   ├── pem v3.0.3
│   │   │   └── base64 v0.21.7
│   │   ├── ring v0.17.7 (*)
│   │   ├── serde v1.0.196 (*)
│   │   ├── serde_json v1.0.113 (*)
│   │   └── simple_asn1 v0.6.2
│   │       ├── num-bigint v0.4.4
│   │       │   ├── num-integer v0.1.46 (*)
│   │       │   └── num-traits v0.2.18 (*)
│   │       │   [build-dependencies]
│   │       │   └── autocfg v1.1.0
│   │       ├── num-traits v0.2.18 (*)
│   │       ├── thiserror v1.0.57 (*)
│   │       └── time v0.3.34 (*)
│   ├── kustos v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos) (*)
│   ├── lapin-pool v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/lapin-pool) (*)
│   ├── log v0.4.20 (*)
│   ├── md5 v0.7.0
│   ├── mime v0.3.17
│   ├── nix v0.27.1
│   │   ├── bitflags v2.4.2
│   │   ├── cfg-if v1.0.0
│   │   └── libc v0.2.153
│   ├── openidconnect v3.4.0 (*)
│   ├── opentalk-cache v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/cache)
│   │   ├── bincode v1.3.3 (*)
│   │   ├── moka v0.12.5
│   │   │   ├── async-lock v2.8.0 (*)
│   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   ├── crossbeam-channel v0.5.11 (*)
│   │   │   ├── crossbeam-epoch v0.9.18
│   │   │   │   └── crossbeam-utils v0.8.19
│   │   │   ├── crossbeam-utils v0.8.19
│   │   │   ├── futures-util v0.3.30 (*)
│   │   │   ├── once_cell v1.19.0
│   │   │   ├── parking_lot v0.12.1 (*)
│   │   │   ├── quanta v0.12.2
│   │   │   │   ├── crossbeam-utils v0.8.19
│   │   │   │   ├── libc v0.2.153
│   │   │   │   ├── once_cell v1.19.0
│   │   │   │   └── raw-cpuid v11.0.1
│   │   │   │       └── bitflags v2.4.2
│   │   │   ├── smallvec v1.13.1 (*)
│   │   │   ├── tagptr v0.2.0
│   │   │   ├── thiserror v1.0.57 (*)
│   │   │   ├── triomphe v0.1.11
│   │   │   └── uuid v1.7.0 (*)
│   │   ├── redis v0.24.0 (*)
│   │   ├── serde v1.0.196 (*)
│   │   ├── siphasher v1.0.0
│   │   └── thiserror v1.0.57 (*)
│   ├── opentalk-controller-settings v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-settings) (*)
│   ├── opentalk-controller-utils v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-utils)
│   │   ├── anyhow v1.0.79
│   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   ├── diesel-async v0.3.2 (*)
│   │   ├── displaydoc v0.2.4 (proc-macro) (*)
│   │   ├── kustos v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos) (*)
│   │   ├── kustos-shared v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos-shared) (*)
│   │   ├── log v0.4.20 (*)
│   │   ├── opentalk-controller-settings v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-settings) (*)
│   │   ├── opentalk-database v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/database) (*)
│   │   ├── opentalk-db-storage v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/db-storage) (*)
│   │   ├── opentalk-log v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/log)
│   │   │   └── log v0.4.20 (*)
│   │   ├── opentalk-nextcloud-client v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/nextcloud-client)
│   │   │   ├── chrono v0.4.34 (*)
│   │   │   ├── derive_more v0.99.17 (proc-macro) (*)
│   │   │   ├── log v0.4.20 (*)
│   │   │   ├── reqwest v0.11.24 (*)
│   │   │   ├── reqwest_dav v0.1.9
│   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   ├── chrono v0.4.34 (*)
│   │   │   │   ├── digest_auth v0.3.1
│   │   │   │   │   ├── digest v0.10.7 (*)
│   │   │   │   │   ├── hex v0.4.3
│   │   │   │   │   ├── md-5 v0.10.6 (*)
│   │   │   │   │   ├── rand v0.8.5 (*)
│   │   │   │   │   └── sha2 v0.10.8 (*)
│   │   │   │   ├── http v0.2.11 (*)
│   │   │   │   ├── httpdate v1.0.3
│   │   │   │   ├── reqwest v0.11.24 (*)
│   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   ├── serde-xml-rs v0.6.0
│   │   │   │   │   ├── log v0.4.20 (*)
│   │   │   │   │   ├── serde v1.0.196 (*)
│   │   │   │   │   ├── thiserror v1.0.57 (*)
│   │   │   │   │   └── xml-rs v0.8.19
│   │   │   │   ├── serde_derive v1.0.196 (proc-macro) (*)
│   │   │   │   ├── serde_json v1.0.113 (*)
│   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   └── url v2.5.0 (*)
│   │   │   ├── serde v1.0.196 (*)
│   │   │   ├── strum v0.26.1 (*)
│   │   │   ├── thiserror v1.0.57 (*)
│   │   │   └── url v2.5.0 (*)
│   │   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│   │   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   │   ├── serde_json v1.0.113 (*)
│   │   └── thiserror v1.0.57 (*)
│   ├── opentalk-database v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/database) (*)
│   ├── opentalk-db-storage v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/db-storage) (*)
│   ├── opentalk-janus-media v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/janus-media) (*)
│   ├── opentalk-jobs v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/jobs)
│   │   ├── anyhow v1.0.79
│   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   ├── chrono v0.4.34 (*)
│   │   ├── displaydoc v0.2.4 (proc-macro) (*)
│   │   ├── kustos v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/kustos) (*)
│   │   ├── log v0.4.20 (*)
│   │   ├── opentalk-controller-settings v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-settings) (*)
│   │   ├── opentalk-controller-utils v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/controller-utils) (*)
│   │   ├── opentalk-database v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/database) (*)
│   │   ├── opentalk-db-storage v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/db-storage) (*)
│   │   ├── opentalk-log v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/log) (*)
│   │   ├── opentalk-nextcloud-client v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/nextcloud-client) (*)
│   │   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│   │   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   │   ├── serde v1.0.196 (*)
│   │   ├── serde_json v1.0.113 (*)
│   │   ├── thiserror v1.0.57 (*)
│   │   └── tokio v1.36.0 (*)
│   ├── opentalk-keycloak-admin v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/keycloak-admin)
│   │   ├── log v0.4.20 (*)
│   │   ├── reqwest v0.11.24 (*)
│   │   ├── serde v1.0.196 (*)
│   │   ├── serde_json v1.0.113 (*)
│   │   ├── thiserror v1.0.57 (*)
│   │   ├── tokio v1.36.0 (*)
│   │   └── url v2.5.0 (*)
│   ├── opentalk-mail-worker-protocol v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/mail-worker-protocol)
│   │   ├── chrono v0.4.34 (*)
│   │   ├── opentalk-db-storage v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/db-storage) (*)
│   │   ├── opentalk-keycloak-admin v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/keycloak-admin) (*)
│   │   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   │   ├── serde v1.0.196 (*)
│   │   └── uuid v1.7.0 (*)
│   ├── opentalk-nextcloud-client v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/nextcloud-client) (*)
│   ├── opentalk-r3dlock v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/r3dlock) (*)
│   ├── opentalk-signaling-core v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/signaling-core) (*)
│   ├── opentalk-types v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/types) (*)
│   ├── opentelemetry v0.21.0 (*)
│   ├── opentelemetry-otlp v0.14.0
│   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   ├── futures-core v0.3.30
│   │   ├── http v0.2.11 (*)
│   │   ├── opentelemetry v0.21.0 (*)
│   │   ├── opentelemetry-proto v0.4.0
│   │   │   ├── opentelemetry v0.21.0 (*)
│   │   │   ├── opentelemetry_sdk v0.21.2
│   │   │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │   │   ├── crossbeam-channel v0.5.11 (*)
│   │   │   │   ├── futures-channel v0.3.30 (*)
│   │   │   │   ├── futures-executor v0.3.30 (*)
│   │   │   │   ├── futures-util v0.3.30 (*)
│   │   │   │   ├── glob v0.3.1
│   │   │   │   ├── once_cell v1.19.0
│   │   │   │   ├── opentelemetry v0.21.0 (*)
│   │   │   │   ├── ordered-float v4.2.0
│   │   │   │   │   └── num-traits v0.2.18 (*)
│   │   │   │   ├── percent-encoding v2.3.1
│   │   │   │   ├── rand v0.8.5 (*)
│   │   │   │   ├── thiserror v1.0.57 (*)
│   │   │   │   ├── tokio v1.36.0 (*)
│   │   │   │   └── tokio-stream v0.1.14 (*)
│   │   │   ├── prost v0.11.9
│   │   │   │   ├── bytes v1.5.0
│   │   │   │   └── prost-derive v0.11.9 (proc-macro)
│   │   │   │       ├── anyhow v1.0.79
│   │   │   │       ├── itertools v0.10.5
│   │   │   │       │   └── either v1.10.0
│   │   │   │       ├── proc-macro2 v1.0.78 (*)
│   │   │   │       ├── quote v1.0.35 (*)
│   │   │   │       └── syn v1.0.109 (*)
│   │   │   └── tonic v0.9.2
│   │   │       ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │       ├── axum v0.6.20
│   │   │       │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │       │   ├── axum-core v0.3.4
│   │   │       │   │   ├── async-trait v0.1.77 (proc-macro) (*)
│   │   │       │   │   ├── bytes v1.5.0
│   │   │       │   │   ├── futures-util v0.3.30 (*)
│   │   │       │   │   ├── http v0.2.11 (*)
│   │   │       │   │   ├── http-body v0.4.6 (*)
│   │   │       │   │   ├── mime v0.3.17
│   │   │       │   │   ├── tower-layer v0.3.2
│   │   │       │   │   └── tower-service v0.3.2
│   │   │       │   │   [build-dependencies]
│   │   │       │   │   └── rustversion v1.0.14 (proc-macro)
│   │   │       │   ├── bitflags v1.3.2
│   │   │       │   ├── bytes v1.5.0
│   │   │       │   ├── futures-util v0.3.30 (*)
│   │   │       │   ├── http v0.2.11 (*)
│   │   │       │   ├── http-body v0.4.6 (*)
│   │   │       │   ├── hyper v0.14.28 (*)
│   │   │       │   ├── itoa v1.0.10
│   │   │       │   ├── matchit v0.7.3
│   │   │       │   ├── memchr v2.7.1
│   │   │       │   ├── mime v0.3.17
│   │   │       │   ├── percent-encoding v2.3.1
│   │   │       │   ├── pin-project-lite v0.2.13
│   │   │       │   ├── serde v1.0.196 (*)
│   │   │       │   ├── sync_wrapper v0.1.2
│   │   │       │   ├── tower v0.4.13
│   │   │       │   │   ├── futures-core v0.3.30
│   │   │       │   │   ├── futures-util v0.3.30 (*)
│   │   │       │   │   ├── indexmap v1.9.3
│   │   │       │   │   │   └── hashbrown v0.12.3
│   │   │       │   │   │   [build-dependencies]
│   │   │       │   │   │   └── autocfg v1.1.0
│   │   │       │   │   ├── pin-project v1.1.4 (*)
│   │   │       │   │   ├── pin-project-lite v0.2.13
│   │   │       │   │   ├── rand v0.8.5 (*)
│   │   │       │   │   ├── slab v0.4.9 (*)
│   │   │       │   │   ├── tokio v1.36.0 (*)
│   │   │       │   │   ├── tokio-util v0.7.10 (*)
│   │   │       │   │   ├── tower-layer v0.3.2
│   │   │       │   │   ├── tower-service v0.3.2
│   │   │       │   │   └── tracing v0.1.40 (*)
│   │   │       │   ├── tower-layer v0.3.2
│   │   │       │   └── tower-service v0.3.2
│   │   │       │   [build-dependencies]
│   │   │       │   └── rustversion v1.0.14 (proc-macro)
│   │   │       ├── base64 v0.21.7
│   │   │       ├── bytes v1.5.0
│   │   │       ├── futures-core v0.3.30
│   │   │       ├── futures-util v0.3.30 (*)
│   │   │       ├── h2 v0.3.24 (*)
│   │   │       ├── http v0.2.11 (*)
│   │   │       ├── http-body v0.4.6 (*)
│   │   │       ├── hyper v0.14.28 (*)
│   │   │       ├── hyper-timeout v0.4.1
│   │   │       │   ├── hyper v0.14.28 (*)
│   │   │       │   ├── pin-project-lite v0.2.13
│   │   │       │   ├── tokio v1.36.0 (*)
│   │   │       │   └── tokio-io-timeout v1.2.0
│   │   │       │       ├── pin-project-lite v0.2.13
│   │   │       │       └── tokio v1.36.0 (*)
│   │   │       ├── percent-encoding v2.3.1
│   │   │       ├── pin-project v1.1.4 (*)
│   │   │       ├── prost v0.11.9 (*)
│   │   │       ├── tokio v1.36.0 (*)
│   │   │       ├── tokio-stream v0.1.14 (*)
│   │   │       ├── tower v0.4.13 (*)
│   │   │       ├── tower-layer v0.3.2
│   │   │       ├── tower-service v0.3.2
│   │   │       └── tracing v0.1.40 (*)
│   │   ├── opentelemetry-semantic-conventions v0.13.0
│   │   │   └── opentelemetry v0.21.0 (*)
│   │   ├── opentelemetry_sdk v0.21.2 (*)
│   │   ├── prost v0.11.9 (*)
│   │   ├── thiserror v1.0.57 (*)
│   │   ├── tokio v1.36.0 (*)
│   │   └── tonic v0.9.2 (*)
│   ├── opentelemetry-prometheus v0.14.1
│   │   ├── once_cell v1.19.0
│   │   ├── opentelemetry v0.21.0 (*)
│   │   ├── opentelemetry_sdk v0.21.2 (*)
│   │   ├── prometheus v0.13.3
│   │   │   ├── cfg-if v1.0.0
│   │   │   ├── fnv v1.0.7
│   │   │   ├── lazy_static v1.4.0 (*)
│   │   │   ├── memchr v2.7.1
│   │   │   ├── parking_lot v0.12.1 (*)
│   │   │   ├── protobuf v2.28.0
│   │   │   └── thiserror v1.0.57 (*)
│   │   └── protobuf v2.28.0
│   ├── opentelemetry_sdk v0.21.2 (*)
│   ├── owo-colors v4.0.0
│   ├── phonenumber v0.3.3+8.13.9 (*)
│   ├── prometheus v0.13.3 (*)
│   ├── rand v0.8.5 (*)
│   ├── redis v0.24.0 (*)
│   ├── redis-args v0.10.0 (/home/silwol/Projects/opentalk/backend/services/controller/crates/redis-args) (*)
│   ├── reqwest v0.11.24 (*)
│   ├── ring v0.17.7 (*)
│   ├── rrule v0.11.0
│   │   ├── chrono v0.4.34 (*)
│   │   ├── chrono-tz v0.8.6 (*)
│   │   ├── lazy_static v1.4.0 (*)
│   │   ├── log v0.4.20 (*)
│   │   ├── regex v1.10.3 (*)
│   │   └── thiserror v1.0.57 (*)
│   ├── rustls v0.21.10 (*)
│   ├── rustls-pemfile v1.0.4 (*)
│   ├── serde v1.0.196 (*)
│   ├── serde_json v1.0.113 (*)
│   ├── sysinfo v0.30.5
│   │   ├── cfg-if v1.0.0
│   │   ├── libc v0.2.153
│   │   └── once_cell v1.19.0
│   ├── tabled v0.15.0
│   │   ├── papergrid v0.11.0
│   │   │   ├── bytecount v0.6.7
│   │   │   ├── fnv v1.0.7
│   │   │   └── unicode-width v0.1.11
│   │   ├── tabled_derive v0.7.0 (proc-macro)
│   │   │   ├── heck v0.4.1
│   │   │   ├── proc-macro-error v1.0.4 (*)
│   │   │   ├── proc-macro2 v1.0.78 (*)
│   │   │   ├── quote v1.0.35 (*)
│   │   │   └── syn v1.0.109 (*)
│   │   └── unicode-width v0.1.11
│   ├── thiserror v1.0.57 (*)
│   ├── tokio v1.36.0 (*)
│   ├── tokio-stream v0.1.14 (*)
│   ├── tracing v0.1.40 (*)
│   ├── tracing-actix-web v0.7.9
│   │   ├── actix-web v4.5.1 (*)
│   │   ├── mutually_exclusive_features v0.0.3
│   │   ├── pin-project v1.1.4 (*)
│   │   ├── tracing v0.1.40 (*)
│   │   └── uuid v1.7.0 (*)
│   ├── tracing-futures v0.2.5 (*)
│   ├── tracing-opentelemetry v0.22.0
│   │   ├── once_cell v1.19.0
│   │   ├── opentelemetry v0.21.0 (*)
│   │   ├── opentelemetry_sdk v0.21.2 (*)
│   │   ├── smallvec v1.13.1 (*)
│   │   ├── tracing v0.1.40 (*)
│   │   ├── tracing-core v0.1.32 (*)
│   │   ├── tracing-log v0.2.0
│   │   │   ├── log v0.4.20 (*)
│   │   │   ├── once_cell v1.19.0
│   │   │   └── tracing-core v0.1.32 (*)
│   │   └── tracing-subscriber v0.3.18
│   │       ├── matchers v0.1.0
│   │       │   └── regex-automata v0.1.10
│   │       │       └── regex-syntax v0.6.29
│   │       ├── nu-ansi-term v0.46.0
│   │       │   └── overload v0.1.1
│   │       ├── once_cell v1.19.0
│   │       ├── parking_lot v0.12.1 (*)
│   │       ├── regex v1.10.3 (*)
│   │       ├── sharded-slab v0.1.7
│   │       │   └── lazy_static v1.4.0 (*)
│   │       ├── smallvec v1.13.1 (*)
│   │       ├── thread_local v1.1.7
│   │       │   ├── cfg-if v1.0.0
│   │       │   └── once_cell v1.19.0
│   │       ├── tracing v0.1.40 (*)
│   │       ├── tracing-core v0.1.32 (*)
│   │       └── tracing-log v0.2.0 (*)
│   ├── tracing-subscriber v0.3.18 (*)
│   ├── url v2.5.0 (*)
│   ├── uuid v1.7.0 (*)
│   └── validator v0.16.1 (*)
│   [build-dependencies]
│   ├── anyhow v1.0.79
│   └── vergen v8.3.1
│       ├── anyhow v1.0.79
│       ├── cargo_metadata v0.18.1
│       │   ├── camino v1.1.6
│       │   │   └── serde v1.0.196 (*)
│       │   ├── cargo-platform v0.1.7
│       │   │   └── serde v1.0.196 (*)
│       │   ├── semver v1.0.21 (*)
│       │   ├── serde v1.0.196 (*)
│       │   ├── serde_json v1.0.113
│       │   │   ├── itoa v1.0.10
│       │   │   ├── ryu v1.0.16
│       │   │   └── serde v1.0.196 (*)
│       │   └── thiserror v1.0.57 (*)
│       ├── cfg-if v1.0.0
│       ├── git2 v0.18.2
│       │   ├── bitflags v2.4.2
│       │   ├── libc v0.2.153
│       │   ├── libgit2-sys v0.16.2+1.7.2
│       │   │   ├── libc v0.2.153
│       │   │   └── libz-sys v1.1.15
│       │   │       └── libc v0.2.153
│       │   │       [build-dependencies]
│       │   │       ├── cc v1.0.83 (*)
│       │   │       ├── pkg-config v0.3.30
│       │   │       └── vcpkg v0.2.15
│       │   │   [build-dependencies]
│       │   │   ├── cc v1.0.83 (*)
│       │   │   └── pkg-config v0.3.30
│       │   ├── log v0.4.20
│       │   └── url v2.5.0 (*)
│       ├── regex v1.10.3 (*)
│       ├── rustc_version v0.4.0 (*)
│       └── time v0.3.34 (*)
│       [build-dependencies]
│       └── rustversion v1.0.14 (proc-macro)
└── owo-colors v4.0.0
```
