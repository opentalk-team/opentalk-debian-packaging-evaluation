# Notes on evaluation of packaging OpenTalk for Debian

I'm currently evaluating how feasible it is to package the community edition of
[OpenTalk Web-Conference Software](https://opentalk.eu/). This document collects
the information gathered during the evaluation. The source code of OpenTalk is
[mirrored to OpenCoDE](https://gitlab.opencode.de/opentalk) whenever a commit
is merged to the `main` branch of the respective repositories.

The community edition of OpenTalk is entirely licensed under the `EUPL-1.2`.
Some reusable components are licensed under `MIT OR Apache-2.0`.

This project lives in [salsa.debian.org/opentalk-team/opentalk-debian-packaging-evaluation](https://salsa.debian.org/opentalk-team/opentalk-debian-packaging-evaluation),
merge requests welcome. Please look at the git history there if you'd like to
know when the contents were last updated.

The contents of this repository are licensed under
[`CC-BY-SA-4.0`](https://creativecommons.org/licenses/by-sa/4.0/) unless noted
otherwise.

## Project structure

OpenTalk consists of several components that are published independently, some
of which are required, others that are optional and provide extra functionality.

Because the components are developed and published independently, their version
numbers differ even for one product release of OpenTalk. The compositions of
component versions that comprise a specific product release can be found on the
[OpenTalk releases page](https://docs.opentalk.eu/releases/). The respective
components have a `v<x>.<y>.<z>` release tag in their repository for each
released version.

This is an overview of the components that exist, and external software that is
required or can be integrated with OpenTalk.

### OpenTalk Web-Frontend

Git repository: https://gitlab.opencode.de/opentalk/web-frontend

This is currently mandatory for using OpenTalk, the artifact that is generated
by the current CI build jobs is a directory containing at least the following
(analyzed from the `v1.11.0` image with manifest digest
`sha256:fa75cdbafebd0122db60d38420d5d186c86436d801c81b2978ff69279580e817` on the
[web-frontend container repository](https://gitlab.opencode.de/opentalk/web-frontend/container_registry/)):

- HTML files
- JavaScript files generated from the TypeScript code, plus corresponding source maps
- CSS files
- PNG Image files (licensed CC-BY-NC-ND-4.0, may be partially generated
  from other formats and checked into the repository) Font files (licensed
  CC-BY-NC-ND-4.0)
- Quck-Start guide in the form of SVG files that were probably
  exported from some PDF file. No idea though about the origin of this PDF file,
  some research might be necessary
- A `robots.txt` file
- Fluent locale files (with `.ftl` suffix)
- SVG files (mostly icons used in UI elements)
- A `tflite` build including `.wasm` binaries and `.js` bindings (no idea about
  their origin, doesn't look like the CI pipeline builds them, but instead they
  were downloaded from somewhere and [checked into the repository](https://gitlab.opencode.de/opentalk/web-frontend/-/tree/main/app/public/tflite?ref_type=heads)).
  (authored by Google according to the license file, but no hint about the upstream project location)
- A `tflite` model in binary form (authored by Google according to the license file, but no hint about the upstream project location)
- A `workers` directory with some `.js` files. Looks like some of them are
  directly implemented in JavaScript by OpenTalk, while others appear to be
  coming from a different project named `Measurement Lab` according to the
  copyright headers.
- Some `.license` files that were added for annotating license information in a
  `reuse`-compatible manner.

The output of the `tree` command used for getting an idea of the contents
is [documented here](web-frontend-artifacts.md).

I'm not a web developer, so probably somebody else would have to fill in the
details about this project. To my knowledge it is a rather regular TypeScript
project, but as usually with large projects, there may be some impassibilities
hidden deep in the dependency tree.

### OpenTalk Controller

Git respository: https://gitlab.opencode.de/opentalk/controller

This is the backend component that serves the web api for the frontend, and
implements the signaling during a web meeting through websockets.

It interacts with the following services:

- A `redis` service for synchronizing the meeting state between multiple
  instances of the `opentalk-controller`. Mandatory.
- A `postgres` database for storing persistent data such as meeting rooms, event
  schedules, invitations or permissions. Mandatory.
- A `rabbitmq` service for interacting with other `opentalk-controller`
  instances in the cluster as well as the `janus` videobridge, the
  `opentalk-recorder` and the `opentalk-smtp-mailer` mentioned below. Mandatory.
- A `janus` videobridge which performs the WebRTC communication with the web
  frontend clients during a meeting. Mandatory.
- An OIDC provider. Currently restricted to Keycloak due to some use of specific
  paths and integration of its web api for user search, but the backend team
  is working on removing this requirement so that any OIDC provider which can serve
  certain attributes can be used. Mandatory.
- A S3-compatible storage. Currently restricted to MinIO due to some implementation
  details, but here the backend team also plans to remove that restriction.
  Currently mandatory, but will probably become optional in the future.
- STUN and TURN servers (no direct interaction, but information about the
  servers that can be used by the web frontend is
  [configured in the `opentalk-controller`](https://docs.opentalk.eu/admin/controller/core/stun_turn/)
  and served through web api endpoints. Optional.
- `opentalk-recorder` which can be used to record and/or live-stream meetings.
  Described in a separate section below. Optional.
- `opentalk-smtp-mailer` which is used for sending out invitations and other
  E-Mail messages to participants. Optional.
- `opentalk-obelisk` which is used for providing phone call-in functionality
  for meetings. Optional.
- `nextcloud` which can be used to share documents during a meeting. Optional.
- `terdoc` which will be a new service developed by the OpenTalk backend team.
   This is planned to become a service that can be used for rendering documents
   when handed a [`tera`](https://keats.github.io/tera/) template, the raw input
   data in `JSON` format and some metadata. The output can be different formats,
   e.g. `.pdf`, `.ods` or `.docx`. The processing for this service will probably
   be delegated to `pandoc`. Will probably become mandatory.
- Etherpad for collaborative editing of meeting notes. Optional.
- SpaceDeck for collaborative drawing on a whiteboard. Optional.

The artifact of the build process is a single executable built with rust.

An analysis of the dependency tree can be found [here](opentalk-controller-dependency-tree.md).

### OpenTalk SMTP-Mailer

Git repository: https://gitlab.opencode.de/opentalk/smtp-mailer

This component sends out E-Mails to invited participants.

An analysis of the dependency tree can be found [here](opentalk-smtp-mailer-dependency-tree.md).

### OpenTalk Obelisk

Git repository: https://gitlab.opencode.de/opentalk/obelisk

This component is used to allow participants joining from a phone through SIP.

An analysis of the dependency tree can be found [here](opentalk-obelisk-dependency-tree.md).

### OpenTalk Recorder

Git repository: https://gitlab.opencode.de/opentalk/recorder

This component is used for recording or live-streaming OpenTalk meetings. It is in
its early stages, so the dependency tree might change significantly in the future.

An analysis of the dependency tree can be found [here](opentalk-recorder-dependency-tree.md).
